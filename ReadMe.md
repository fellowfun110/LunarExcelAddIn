此项目是一个用 VSTO 开发的、简单的 Excel 2016 插件程序。

主要用以集成一些常用操作。

> 理论上适用于 Excel 2016 和 Excel 2013 版。

![主要功能](https://git.oschina.net/uploads/images/2017/0921/065927_a119801f_310357.png "屏幕截图.png")