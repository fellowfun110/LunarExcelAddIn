﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace LunarExcelAddIn
{
    public partial class RegexInputBox : Form
    {
        public RegexInputBox(string prompt = null, string btnText = null)
        {
            InitializeComponent();

            if (string.IsNullOrEmpty(prompt))
            {
                lblPrompt.Text = "";
                lblPrompt.Visible = false;
            }
            else
            {
                lblPrompt.Text = prompt;
                lblPrompt.Visible = true;
            }

            if (string.IsNullOrWhiteSpace(btnText) == false)
            {
                btnValidate.Text = btnText;
            }
        }

        public static Microsoft.Office.Interop.Excel.Application App { get { return Globals.ThisAddIn.Application; } }

        /// <summary>
        /// 将输入的正则表达式添加到临时列表中。
        /// </summary>
        private void KeeyRegexes(string regText)
        {
            if (string.IsNullOrEmpty(regText))
            {
                return;
            }

            int index = -1;
            for (int i = 0; i < cmbRegexInputBox.Items.Count; i++)
            {
                if (cmbRegexInputBox.Items[i].ToString() == regText)
                {
                    index = i;
                    break;
                }
            }

            if (index >= 0)
            {
                cmbRegexInputBox.Items.RemoveAt(index);
            }

            cmbRegexInputBox.Items.Insert(0, regText);
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cmbRegexInputBox.Text))
            {
                MessageBox.Show("请输入正则表达式！");
                return;
            }

            KeeyRegexes(cmbRegexInputBox.Text);

            Excel.Worksheet asheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (asheet == null)
            {
                MessageBox.Show("没有找到活动工作表！", "LunarExcelAddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Regex reg = new Regex(cmbRegexInputBox.Text);

            Range seledRngs = App.Selection;
            int textCellCount = 0;
            int nonTextCellCount = 0;
            int validatedCount = 0;
            int cellCount = 0;
            foreach (Excel.Range rng in seledRngs)
            {
                cellCount++;
                if (App.WorksheetFunction.IsText(rng))
                {
                    textCellCount++;

                    if (rng.Value == null)
                    {
                        rng.Interior.ColorIndex = 0;
                        continue;
                    }

                    Match match = reg.Match(rng.Value.ToString());
                    if (match.Success)
                    {
                        validatedCount++;
                        rng.Interior.ColorIndex = 35;   // 灰绿色
                    }
                    else
                    {
                        rng.Interior.ColorIndex = 6;  // 黄色
                    }
                }
                else
                {
                    nonTextCellCount++;
                }
            }

            MessageBox.Show($"选定单元格【{cellCount}】个，其中：\r\n" +
                $"　-------------------\r\n" +
                $"　非文本格式单元格：【{cellCount - textCellCount}】个；\r\n" +
                $"　-------------------\r\n" +
                $"　　文本格式单元格：【{textCellCount}】个，其中：\r\n" +
                $"　　　未通过验证的：【{textCellCount - validatedCount}】个〔黄色〕；\r\n" +
                $"　　　　通过验证的：【{validatedCount}】个〔绿色〕。", "LunarExcelAddIn",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void RegexInputBox_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbRegexInputBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnValidate_Click(sender, e);
            }
        }
    }
}
