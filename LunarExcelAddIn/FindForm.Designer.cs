﻿namespace LunarExcelAddIn
{
    partial class FindForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.ckxFstLineIsTitle = new System.Windows.Forms.CheckBox();
            this.ckxIgnoreCase = new System.Windows.Forms.CheckBox();
            this.btnQuickFind = new System.Windows.Forms.Button();
            this.cmbQueryFeilds = new System.Windows.Forms.ComboBox();
            this.cmbFindColumn = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxQueryLines = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSwitchSelection = new System.Windows.Forms.Button();
            this.btnUnSelectAll = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.cklstColumns = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbSheetNames = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnPasteToNewSheet = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1023, 652);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Size = new System.Drawing.Size(1015, 619);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "查找条件";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnClear);
            this.groupBox2.Controls.Add(this.ckxFstLineIsTitle);
            this.groupBox2.Controls.Add(this.ckxIgnoreCase);
            this.groupBox2.Controls.Add(this.btnQuickFind);
            this.groupBox2.Controls.Add(this.cmbQueryFeilds);
            this.groupBox2.Controls.Add(this.cmbFindColumn);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.tbxQueryLines);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(487, 10);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(518, 598);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "查找目标";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnClear.Location = new System.Drawing.Point(397, 452);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(100, 32);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "清除(&C)";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ckxFstLineIsTitle
            // 
            this.ckxFstLineIsTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ckxFstLineIsTitle.AutoSize = true;
            this.ckxFstLineIsTitle.Checked = true;
            this.ckxFstLineIsTitle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckxFstLineIsTitle.Location = new System.Drawing.Point(191, 554);
            this.ckxFstLineIsTitle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckxFstLineIsTitle.Name = "ckxFstLineIsTitle";
            this.ckxFstLineIsTitle.Size = new System.Drawing.Size(116, 24);
            this.ckxFstLineIsTitle.TabIndex = 10;
            this.ckxFstLineIsTitle.Text = "首行是标题(&T)";
            this.ckxFstLineIsTitle.UseVisualStyleBackColor = true;
            // 
            // ckxIgnoreCase
            // 
            this.ckxIgnoreCase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ckxIgnoreCase.AutoSize = true;
            this.ckxIgnoreCase.Location = new System.Drawing.Point(35, 554);
            this.ckxIgnoreCase.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckxIgnoreCase.Name = "ckxIgnoreCase";
            this.ckxIgnoreCase.Size = new System.Drawing.Size(112, 24);
            this.ckxIgnoreCase.TabIndex = 10;
            this.ckxIgnoreCase.Text = "忽略大小写(&I)";
            this.ckxIgnoreCase.UseVisualStyleBackColor = true;
            // 
            // btnQuickFind
            // 
            this.btnQuickFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuickFind.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnQuickFind.Location = new System.Drawing.Point(397, 554);
            this.btnQuickFind.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnQuickFind.Name = "btnQuickFind";
            this.btnQuickFind.Size = new System.Drawing.Size(104, 32);
            this.btnQuickFind.TabIndex = 9;
            this.btnQuickFind.Text = "查找(&Q)";
            this.btnQuickFind.UseVisualStyleBackColor = true;
            this.btnQuickFind.Click += new System.EventHandler(this.btnQuickFind_Click);
            // 
            // cmbQueryFeilds
            // 
            this.cmbQueryFeilds.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbQueryFeilds.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueryFeilds.FormattingEnabled = true;
            this.cmbQueryFeilds.Location = new System.Drawing.Point(35, 504);
            this.cmbQueryFeilds.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbQueryFeilds.Name = "cmbQueryFeilds";
            this.cmbQueryFeilds.Size = new System.Drawing.Size(461, 28);
            this.cmbQueryFeilds.TabIndex = 7;
            // 
            // cmbFindColumn
            // 
            this.cmbFindColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbFindColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFindColumn.FormattingEnabled = true;
            this.cmbFindColumn.Location = new System.Drawing.Point(36, 53);
            this.cmbFindColumn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbFindColumn.Name = "cmbFindColumn";
            this.cmbFindColumn.Size = new System.Drawing.Size(461, 28);
            this.cmbFindColumn.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 460);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "请选择查询条件列：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 24);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(261, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "请选择要从数据源表哪一列中查找数据：";
            // 
            // tbxQueryLines
            // 
            this.tbxQueryLines.AcceptsReturn = true;
            this.tbxQueryLines.AcceptsTab = true;
            this.tbxQueryLines.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxQueryLines.BackColor = System.Drawing.Color.Thistle;
            this.tbxQueryLines.ForeColor = System.Drawing.Color.Black;
            this.tbxQueryLines.Location = new System.Drawing.Point(35, 125);
            this.tbxQueryLines.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbxQueryLines.MaxLength = 2147483647;
            this.tbxQueryLines.Multiline = true;
            this.tbxQueryLines.Name = "tbxQueryLines";
            this.tbxQueryLines.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbxQueryLines.Size = new System.Drawing.Size(461, 317);
            this.tbxQueryLines.TabIndex = 5;
            this.tbxQueryLines.TextChanged += new System.EventHandler(this.tbxQueryLines_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(219, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "请将要查询的数据列粘贴到下面：";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.btnSwitchSelection);
            this.groupBox1.Controls.Add(this.btnUnSelectAll);
            this.groupBox1.Controls.Add(this.btnSelectAll);
            this.groupBox1.Controls.Add(this.cklstColumns);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmbSheetNames);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(8, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(465, 598);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "源数据";
            // 
            // btnSwitchSelection
            // 
            this.btnSwitchSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSwitchSelection.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSwitchSelection.Location = new System.Drawing.Point(246, 554);
            this.btnSwitchSelection.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSwitchSelection.Name = "btnSwitchSelection";
            this.btnSwitchSelection.Size = new System.Drawing.Size(100, 32);
            this.btnSwitchSelection.TabIndex = 8;
            this.btnSwitchSelection.Text = "反选(&S)";
            this.btnSwitchSelection.UseVisualStyleBackColor = true;
            this.btnSwitchSelection.Click += new System.EventHandler(this.btnSwitchSelection_Click);
            // 
            // btnUnSelectAll
            // 
            this.btnUnSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUnSelectAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnUnSelectAll.Location = new System.Drawing.Point(136, 554);
            this.btnUnSelectAll.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUnSelectAll.Name = "btnUnSelectAll";
            this.btnUnSelectAll.Size = new System.Drawing.Size(100, 32);
            this.btnUnSelectAll.TabIndex = 7;
            this.btnUnSelectAll.Text = "全不选(&U)";
            this.btnUnSelectAll.UseVisualStyleBackColor = true;
            this.btnUnSelectAll.Click += new System.EventHandler(this.btnUnSelectAll_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSelectAll.Location = new System.Drawing.Point(27, 554);
            this.btnSelectAll.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(100, 32);
            this.btnSelectAll.TabIndex = 6;
            this.btnSelectAll.Text = "全选(&A)";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // cklstColumns
            // 
            this.cklstColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cklstColumns.FormattingEnabled = true;
            this.cklstColumns.Location = new System.Drawing.Point(24, 200);
            this.cklstColumns.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cklstColumns.Name = "cklstColumns";
            this.cklstColumns.Size = new System.Drawing.Size(408, 340);
            this.cklstColumns.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 165);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(205, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "请选择需要显示在结果中的列：";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown1.Location = new System.Drawing.Point(24, 125);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(412, 26);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 96);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(191, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "请输入列标题所在行的行号：";
            // 
            // cmbSheetNames
            // 
            this.cmbSheetNames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbSheetNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSheetNames.FormattingEnabled = true;
            this.cmbSheetNames.Location = new System.Drawing.Point(25, 53);
            this.cmbSheetNames.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbSheetNames.Name = "cmbSheetNames";
            this.cmbSheetNames.Size = new System.Drawing.Size(411, 28);
            this.cmbSheetNames.TabIndex = 1;
            this.cmbSheetNames.SelectedIndexChanged += new System.EventHandler(this.cmbSheetNames_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "请选择源数据所在工作表：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnPasteToNewSheet);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Size = new System.Drawing.Size(1015, 619);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "查找结果";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnPasteToNewSheet
            // 
            this.btnPasteToNewSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPasteToNewSheet.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPasteToNewSheet.Location = new System.Drawing.Point(786, 575);
            this.btnPasteToNewSheet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPasteToNewSheet.Name = "btnPasteToNewSheet";
            this.btnPasteToNewSheet.Size = new System.Drawing.Size(219, 32);
            this.btnPasteToNewSheet.TabIndex = 1;
            this.btnPasteToNewSheet.Text = "粘贴到新工作表(&P)";
            this.btnPasteToNewSheet.UseVisualStyleBackColor = true;
            this.btnPasteToNewSheet.Click += new System.EventHandler(this.btnPasteToNewSheet_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(4, 5);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1001, 555);
            this.dataGridView1.StandardTab = true;
            this.dataGridView1.TabIndex = 0;
            this.toolTip1.SetToolTip(this.dataGridView1, "可拖动标题校调整位置");
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipTitle = "输入文本后按 Ctrl+Enter";
            // 
            // FindForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 651);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FindForm";
            this.Text = "按列查找";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnPasteToNewSheet;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.CheckBox ckxFstLineIsTitle;
        private System.Windows.Forms.CheckBox ckxIgnoreCase;
        private System.Windows.Forms.Button btnQuickFind;
        private System.Windows.Forms.ComboBox cmbQueryFeilds;
        private System.Windows.Forms.ComboBox cmbFindColumn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxQueryLines;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSwitchSelection;
        private System.Windows.Forms.Button btnUnSelectAll;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.CheckedListBox cklstColumns;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbSheetNames;
        private System.Windows.Forms.Label label1;
    }
}