﻿namespace LunarExcelAddIn
{
    partial class AggregateDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.gpDataSource = new System.Windows.Forms.GroupBox();
            this.cmbSheetNames = new System.Windows.Forms.ComboBox();
            this.btnAggregate = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnPasteToNewSheet = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxJoinChars = new System.Windows.Forms.TextBox();
            this.lblJoinChars = new System.Windows.Forms.Label();
            this.rbtnJoin = new System.Windows.Forms.RadioButton();
            this.rbtnSum = new System.Windows.Forms.RadioButton();
            this.rbtnCount = new System.Windows.Forms.RadioButton();
            this.cmbClassFeild = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbNumberFeild = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.gpDataSource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown1.Location = new System.Drawing.Point(27, 136);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(389, 26);
            this.numericUpDown1.TabIndex = 20;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 98);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(191, 20);
            this.label3.TabIndex = 19;
            this.label3.Text = "请输入列标题所在行的行号：";
            // 
            // gpDataSource
            // 
            this.gpDataSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpDataSource.Controls.Add(this.cmbSheetNames);
            this.gpDataSource.Location = new System.Drawing.Point(16, 20);
            this.gpDataSource.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gpDataSource.Name = "gpDataSource";
            this.gpDataSource.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gpDataSource.Size = new System.Drawing.Size(409, 73);
            this.gpDataSource.TabIndex = 18;
            this.gpDataSource.TabStop = false;
            this.gpDataSource.Text = "请指定源数据表：";
            // 
            // cmbSheetNames
            // 
            this.cmbSheetNames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbSheetNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSheetNames.FormattingEnabled = true;
            this.cmbSheetNames.Location = new System.Drawing.Point(11, 29);
            this.cmbSheetNames.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbSheetNames.Name = "cmbSheetNames";
            this.cmbSheetNames.Size = new System.Drawing.Size(389, 28);
            this.cmbSheetNames.Sorted = true;
            this.cmbSheetNames.TabIndex = 0;
            this.cmbSheetNames.SelectedIndexChanged += new System.EventHandler(this.cmbSheetNames_SelectedIndexChanged);
            // 
            // btnAggregate
            // 
            this.btnAggregate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAggregate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAggregate.Location = new System.Drawing.Point(197, 620);
            this.btnAggregate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAggregate.Name = "btnAggregate";
            this.btnAggregate.Size = new System.Drawing.Size(147, 32);
            this.btnAggregate.TabIndex = 23;
            this.btnAggregate.Text = "分类汇总(&A)";
            this.btnAggregate.UseVisualStyleBackColor = true;
            this.btnAggregate.Click += new System.EventHandler(this.btnAggregate_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnClose.Location = new System.Drawing.Point(782, 620);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(140, 32);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "关闭(&X)";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPasteToNewSheet
            // 
            this.btnPasteToNewSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPasteToNewSheet.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPasteToNewSheet.Location = new System.Drawing.Point(434, 620);
            this.btnPasteToNewSheet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPasteToNewSheet.Name = "btnPasteToNewSheet";
            this.btnPasteToNewSheet.Size = new System.Drawing.Size(177, 32);
            this.btnPasteToNewSheet.TabIndex = 25;
            this.btnPasteToNewSheet.Text = "粘贴到新工作表(_P)";
            this.btnPasteToNewSheet.UseVisualStyleBackColor = true;
            this.btnPasteToNewSheet.Click += new System.EventHandler(this.btnPasteToNewSheet_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(433, 20);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(489, 584);
            this.dataGridView1.StandardTab = true;
            this.dataGridView1.TabIndex = 26;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tbxJoinChars);
            this.groupBox1.Controls.Add(this.lblJoinChars);
            this.groupBox1.Controls.Add(this.rbtnJoin);
            this.groupBox1.Controls.Add(this.rbtnSum);
            this.groupBox1.Controls.Add(this.rbtnCount);
            this.groupBox1.Location = new System.Drawing.Point(18, 264);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(398, 0);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "汇总方式";
            // 
            // tbxJoinChars
            // 
            this.tbxJoinChars.Enabled = false;
            this.tbxJoinChars.Location = new System.Drawing.Point(222, 64);
            this.tbxJoinChars.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbxJoinChars.Name = "tbxJoinChars";
            this.tbxJoinChars.Size = new System.Drawing.Size(88, 26);
            this.tbxJoinChars.TabIndex = 6;
            this.tbxJoinChars.Text = " | ";
            // 
            // lblJoinChars
            // 
            this.lblJoinChars.AutoSize = true;
            this.lblJoinChars.Location = new System.Drawing.Point(149, 68);
            this.lblJoinChars.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJoinChars.Name = "lblJoinChars";
            this.lblJoinChars.Size = new System.Drawing.Size(65, 20);
            this.lblJoinChars.TabIndex = 5;
            this.lblJoinChars.Text = "连接符：";
            // 
            // rbtnJoin
            // 
            this.rbtnJoin.AutoSize = true;
            this.rbtnJoin.Location = new System.Drawing.Point(29, 65);
            this.rbtnJoin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtnJoin.Name = "rbtnJoin";
            this.rbtnJoin.Size = new System.Drawing.Size(71, 24);
            this.rbtnJoin.TabIndex = 4;
            this.rbtnJoin.Text = "连接(&J)";
            this.rbtnJoin.UseVisualStyleBackColor = true;
            // 
            // rbtnSum
            // 
            this.rbtnSum.AutoSize = true;
            this.rbtnSum.Checked = true;
            this.rbtnSum.Location = new System.Drawing.Point(29, 29);
            this.rbtnSum.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtnSum.Name = "rbtnSum";
            this.rbtnSum.Size = new System.Drawing.Size(73, 24);
            this.rbtnSum.TabIndex = 2;
            this.rbtnSum.TabStop = true;
            this.rbtnSum.Text = "求和(&S)";
            this.rbtnSum.UseVisualStyleBackColor = true;
            // 
            // rbtnCount
            // 
            this.rbtnCount.AutoSize = true;
            this.rbtnCount.Location = new System.Drawing.Point(152, 29);
            this.rbtnCount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtnCount.Name = "rbtnCount";
            this.rbtnCount.Size = new System.Drawing.Size(74, 24);
            this.rbtnCount.TabIndex = 3;
            this.rbtnCount.Text = "计数(&C)";
            this.rbtnCount.UseVisualStyleBackColor = true;
            // 
            // cmbClassFeild
            // 
            this.cmbClassFeild.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbClassFeild.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClassFeild.FormattingEnabled = true;
            this.cmbClassFeild.Location = new System.Drawing.Point(27, 216);
            this.cmbClassFeild.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbClassFeild.Name = "cmbClassFeild";
            this.cmbClassFeild.Size = new System.Drawing.Size(389, 28);
            this.cmbClassFeild.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 180);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "请选择用于汇总的基准分类字段：";
            // 
            // cmbNumberFeild
            // 
            this.cmbNumberFeild.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbNumberFeild.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNumberFeild.FormattingEnabled = true;
            this.cmbNumberFeild.Location = new System.Drawing.Point(27, 420);
            this.cmbNumberFeild.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbNumberFeild.Name = "cmbNumberFeild";
            this.cmbNumberFeild.Size = new System.Drawing.Size(389, 28);
            this.cmbNumberFeild.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 382);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 20);
            this.label2.TabIndex = 30;
            this.label2.Text = "请选择要汇总的目标字段：";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(18, 462);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(398, 135);
            this.textBox1.TabIndex = 32;
            this.textBox1.Text = "基准字段用于分类（例如各学校）；\r\n\r\n汇总数据字段用于汇总数量（例如给各校的经费）。\r\n\r\n视数据量大小、计算机性能，可能需要数十秒到几分钟不等的时间，请耐心等" +
    "待！！！";
            // 
            // AggregateDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 671);
            this.Controls.Add(this.cmbNumberFeild);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmbClassFeild);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnPasteToNewSheet);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAggregate);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gpDataSource);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "AggregateDialog";
            this.Text = "按字段汇总";
            this.Load += new System.EventHandler(this.AggregateDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.gpDataSource.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gpDataSource;
        private System.Windows.Forms.ComboBox cmbSheetNames;
        private System.Windows.Forms.Button btnAggregate;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnPasteToNewSheet;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbxJoinChars;
        private System.Windows.Forms.Label lblJoinChars;
        private System.Windows.Forms.RadioButton rbtnJoin;
        private System.Windows.Forms.RadioButton rbtnSum;
        private System.Windows.Forms.RadioButton rbtnCount;
        private System.Windows.Forms.ComboBox cmbClassFeild;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbNumberFeild;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
    }
}