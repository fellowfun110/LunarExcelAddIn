﻿namespace LunarExcelAddIn
{
    partial class LunarRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public LunarRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.LunarAddIn = this.Factory.CreateRibbonTab();
            this.gpSelTools = this.Factory.CreateRibbonGroup();
            this.btnSelectEmptyCells = this.Factory.CreateRibbonButton();
            this.btnAddComments = this.Factory.CreateRibbonButton();
            this.btnFindRepeatRecords = this.Factory.CreateRibbonButton();
            this.btnFind = this.Factory.CreateRibbonButton();
            this.gpValidateTools = this.Factory.CreateRibbonGroup();
            this.btnIdCardNumberValidate = this.Factory.CreateRibbonButton();
            this.btnBankCardNumberValidate = this.Factory.CreateRibbonButton();
            this.btnValidateWithRegex = this.Factory.CreateRibbonButton();
            this.gpRowAndColumn = this.Factory.CreateRibbonGroup();
            this.btnSetBackgroundBetweenLines = this.Factory.CreateRibbonButton();
            this.btnSetBackgroundBetweenColumns = this.Factory.CreateRibbonButton();
            this.btnSetAreaBackColor = this.Factory.CreateRibbonButton();
            this.btnSetBackgroundByFirstChar = this.Factory.CreateRibbonButton();
            this.gpFormatCells = this.Factory.CreateRibbonGroup();
            this.btnTrimBlankChars = this.Factory.CreateRibbonButton();
            this.btnRemoveBlankChars = this.Factory.CreateRibbonButton();
            this.btnReplaceBlankChars = this.Factory.CreateRibbonButton();
            this.gpSimpleStatistics = this.Factory.CreateRibbonGroup();
            this.btnAggregateByFeild = this.Factory.CreateRibbonButton();
            this.btnAutoAggregateByFeild = this.Factory.CreateRibbonButton();
            this.gpFormat = this.Factory.CreateRibbonGroup();
            this.btnAddAllBorders = this.Factory.CreateRibbonButton();
            this.btnClearAllBorders = this.Factory.CreateRibbonButton();
            this.btnSetLeftAndRightBorder = this.Factory.CreateRibbonButton();
            this.btnOnlyTopAndBottomBorder = this.Factory.CreateRibbonButton();
            this.gpPageBreak = this.Factory.CreateRibbonGroup();
            this.btnAddPageCutter = this.Factory.CreateRibbonButton();
            this.btnResetPageBreaks = this.Factory.CreateRibbonButton();
            this.gpFileAndFolder = this.Factory.CreateRibbonGroup();
            this.btnSplitByField = this.Factory.CreateRibbonButton();
            this.btnCreateDirectoies = this.Factory.CreateRibbonButton();
            this.btnTags = this.Factory.CreateRibbonButton();
            this.btnMailCop = this.Factory.CreateRibbonButton();
            this.gpOthers = this.Factory.CreateRibbonGroup();
            this.btn阅卷 = this.Factory.CreateRibbonButton();
            this.button1 = this.Factory.CreateRibbonButton();
            this.gpAbout = this.Factory.CreateRibbonGroup();
            this.lblWarning = this.Factory.CreateRibbonLabel();
            this.label2 = this.Factory.CreateRibbonLabel();
            this.label1 = this.Factory.CreateRibbonLabel();
            this.tab1.SuspendLayout();
            this.LunarAddIn.SuspendLayout();
            this.gpSelTools.SuspendLayout();
            this.gpValidateTools.SuspendLayout();
            this.gpRowAndColumn.SuspendLayout();
            this.gpFormatCells.SuspendLayout();
            this.gpSimpleStatistics.SuspendLayout();
            this.gpFormat.SuspendLayout();
            this.gpPageBreak.SuspendLayout();
            this.gpFileAndFolder.SuspendLayout();
            this.gpOthers.SuspendLayout();
            this.gpAbout.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // LunarAddIn
            // 
            this.LunarAddIn.Groups.Add(this.gpSelTools);
            this.LunarAddIn.Groups.Add(this.gpValidateTools);
            this.LunarAddIn.Groups.Add(this.gpRowAndColumn);
            this.LunarAddIn.Groups.Add(this.gpFormatCells);
            this.LunarAddIn.Groups.Add(this.gpSimpleStatistics);
            this.LunarAddIn.Groups.Add(this.gpFormat);
            this.LunarAddIn.Groups.Add(this.gpPageBreak);
            this.LunarAddIn.Groups.Add(this.gpFileAndFolder);
            this.LunarAddIn.Groups.Add(this.gpOthers);
            this.LunarAddIn.Groups.Add(this.gpAbout);
            this.LunarAddIn.KeyTip = "L";
            this.LunarAddIn.Label = "Lunar工具";
            this.LunarAddIn.Name = "LunarAddIn";
            // 
            // gpSelTools
            // 
            this.gpSelTools.Items.Add(this.btnSelectEmptyCells);
            this.gpSelTools.Items.Add(this.btnAddComments);
            this.gpSelTools.Items.Add(this.btnFindRepeatRecords);
            this.gpSelTools.Items.Add(this.btnFind);
            this.gpSelTools.KeyTip = "C";
            this.gpSelTools.Label = "常用";
            this.gpSelTools.Name = "gpSelTools";
            // 
            // btnSelectEmptyCells
            // 
            this.btnSelectEmptyCells.Image = global::LunarExcelAddIn.Properties.Resources.SelEmptyCells;
            this.btnSelectEmptyCells.KeyTip = "S";
            this.btnSelectEmptyCells.Label = "选取空单元格";
            this.btnSelectEmptyCells.Name = "btnSelectEmptyCells";
            this.btnSelectEmptyCells.ScreenTip = "在当前范围内选取空单元格";
            this.btnSelectEmptyCells.ShowImage = true;
            this.btnSelectEmptyCells.SuperTip = "先选取一些单元格，然后点击此按钮，会选中这些单元格中的空单元格。";
            this.btnSelectEmptyCells.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSelectEmptyCells_Click);
            // 
            // btnAddComments
            // 
            this.btnAddComments.Description = "加批注";
            this.btnAddComments.Image = global::LunarExcelAddIn.Properties.Resources.Comment;
            this.btnAddComments.KeyTip = "C";
            this.btnAddComments.Label = "批量批注单元格";
            this.btnAddComments.Name = "btnAddComments";
            this.btnAddComments.ScreenTip = "批量给选定单元格添加批注文本";
            this.btnAddComments.ShowImage = true;
            this.btnAddComments.SuperTip = "注意：会自动添加时间标签，所以不需要手工输入。";
            this.btnAddComments.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAddComments_Click);
            // 
            // btnFindRepeatRecords
            // 
            this.btnFindRepeatRecords.Image = global::LunarExcelAddIn.Properties.Resources.repeat_records;
            this.btnFindRepeatRecords.Label = "记录查重";
            this.btnFindRepeatRecords.Name = "btnFindRepeatRecords";
            this.btnFindRepeatRecords.ScreenTip = "按勾选的列查找重复记录。";
            this.btnFindRepeatRecords.ShowImage = true;
            this.btnFindRepeatRecords.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnFindRepeatRecords_Click);
            // 
            // btnFind
            // 
            this.btnFind.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnFind.Image = global::LunarExcelAddIn.Properties.Resources.Find;
            this.btnFind.KeyTip = "F";
            this.btnFind.Label = "按列查找";
            this.btnFind.Name = "btnFind";
            this.btnFind.ScreenTip = "类似 VLookUp";
            this.btnFind.ShowImage = true;
            this.btnFind.SuperTip = "根据提供的列查找某表中的数据。";
            this.btnFind.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnFind_Click);
            // 
            // gpValidateTools
            // 
            this.gpValidateTools.Items.Add(this.btnIdCardNumberValidate);
            this.gpValidateTools.Items.Add(this.btnBankCardNumberValidate);
            this.gpValidateTools.Items.Add(this.btnValidateWithRegex);
            this.gpValidateTools.KeyTip = "V";
            this.gpValidateTools.Label = "验证";
            this.gpValidateTools.Name = "gpValidateTools";
            // 
            // btnIdCardNumberValidate
            // 
            this.btnIdCardNumberValidate.Image = global::LunarExcelAddIn.Properties.Resources.idcard;
            this.btnIdCardNumberValidate.KeyTip = "I";
            this.btnIdCardNumberValidate.Label = "验证身份证号";
            this.btnIdCardNumberValidate.Name = "btnIdCardNumberValidate";
            this.btnIdCardNumberValidate.ShowImage = true;
            this.btnIdCardNumberValidate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnIdCardNumberValidate_Click);
            // 
            // btnBankCardNumberValidate
            // 
            this.btnBankCardNumberValidate.Image = global::LunarExcelAddIn.Properties.Resources.Card;
            this.btnBankCardNumberValidate.KeyTip = "R";
            this.btnBankCardNumberValidate.Label = "验证银行卡号";
            this.btnBankCardNumberValidate.Name = "btnBankCardNumberValidate";
            this.btnBankCardNumberValidate.ScreenTip = "★仅支持19位卡号";
            this.btnBankCardNumberValidate.ShowImage = true;
            this.btnBankCardNumberValidate.SuperTip = "由于数据来源不全，验证结果仅供参考。";
            this.btnBankCardNumberValidate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnBankCardNumberValidate_Click);
            // 
            // btnValidateWithRegex
            // 
            this.btnValidateWithRegex.Image = global::LunarExcelAddIn.Properties.Resources.script_code;
            this.btnValidateWithRegex.Label = "正则表达式验证";
            this.btnValidateWithRegex.Name = "btnValidateWithRegex";
            this.btnValidateWithRegex.ShowImage = true;
            this.btnValidateWithRegex.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnValidateWithRegex_Click);
            // 
            // gpRowAndColumn
            // 
            this.gpRowAndColumn.Items.Add(this.btnSetBackgroundBetweenLines);
            this.gpRowAndColumn.Items.Add(this.btnSetBackgroundBetweenColumns);
            this.gpRowAndColumn.Items.Add(this.btnSetAreaBackColor);
            this.gpRowAndColumn.Items.Add(this.btnSetBackgroundByFirstChar);
            this.gpRowAndColumn.KeyTip = "R";
            this.gpRowAndColumn.Label = "行列";
            this.gpRowAndColumn.Name = "gpRowAndColumn";
            // 
            // btnSetBackgroundBetweenLines
            // 
            this.btnSetBackgroundBetweenLines.Image = global::LunarExcelAddIn.Properties.Resources.setColorBetweenLines;
            this.btnSetBackgroundBetweenLines.Label = "隔行着色";
            this.btnSetBackgroundBetweenLines.Name = "btnSetBackgroundBetweenLines";
            this.btnSetBackgroundBetweenLines.ScreenTip = "隔行设置背景色";
            this.btnSetBackgroundBetweenLines.ShowImage = true;
            this.btnSetBackgroundBetweenLines.SuperTip = "将偶数行底色设置为浅灰色，奇数行设置为透明色。";
            this.btnSetBackgroundBetweenLines.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSetBackgroundBetweenLines_Click);
            // 
            // btnSetBackgroundBetweenColumns
            // 
            this.btnSetBackgroundBetweenColumns.Image = global::LunarExcelAddIn.Properties.Resources.setColorBetweenColumns;
            this.btnSetBackgroundBetweenColumns.Label = "隔列着色";
            this.btnSetBackgroundBetweenColumns.Name = "btnSetBackgroundBetweenColumns";
            this.btnSetBackgroundBetweenColumns.ShowImage = true;
            this.btnSetBackgroundBetweenColumns.SuperTip = "将偶数列底色设置为浅灰色，奇数列设置为透明色。";
            this.btnSetBackgroundBetweenColumns.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSetBackgroundBetweenColumns_Click);
            // 
            // btnSetAreaBackColor
            // 
            this.btnSetAreaBackColor.Image = global::LunarExcelAddIn.Properties.Resources.color_feilds;
            this.btnSetAreaBackColor.KeyTip = "A";
            this.btnSetAreaBackColor.Label = "分区域设置底色";
            this.btnSetAreaBackColor.Name = "btnSetAreaBackColor";
            this.btnSetAreaBackColor.ScreenTip = "令相似行底色一致";
            this.btnSetAreaBackColor.ShowImage = true;
            this.btnSetAreaBackColor.SuperTip = "例如，01班为透明底色，02班为浅灰底色，03班又是透明，04班又是浅灰，如此类推。此操作要求先选中班级列中所有班级单元格（而不能直接选中整个列）。";
            this.btnSetAreaBackColor.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSetAreaBackColor_Click);
            // 
            // btnSetBackgroundByFirstChar
            // 
            this.btnSetBackgroundByFirstChar.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnSetBackgroundByFirstChar.Image = global::LunarExcelAddIn.Properties.Resources.name;
            this.btnSetBackgroundByFirstChar.KeyTip = "N";
            this.btnSetBackgroundByFirstChar.Label = "首字分区";
            this.btnSetBackgroundByFirstChar.Name = "btnSetBackgroundByFirstChar";
            this.btnSetBackgroundByFirstChar.ScreenTip = "按某字段首字符将某些相邻记录底色设置为相同。";
            this.btnSetBackgroundByFirstChar.ShowImage = true;
            this.btnSetBackgroundByFirstChar.SuperTip = "通常用于按姓名字段首字符纵向分区域设置底色。通常用于人工在一个按姓名排序的班级名册中迅速找到某个学生——因为同一姓氏的在一起，设置同样的底色可以迅速定位。";
            this.btnSetBackgroundByFirstChar.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSetBackgroundByFirstChar_Click);
            // 
            // gpFormatCells
            // 
            this.gpFormatCells.Items.Add(this.btnTrimBlankChars);
            this.gpFormatCells.Items.Add(this.btnRemoveBlankChars);
            this.gpFormatCells.Items.Add(this.btnReplaceBlankChars);
            this.gpFormatCells.Label = "格式化文本";
            this.gpFormatCells.Name = "gpFormatCells";
            // 
            // btnTrimBlankChars
            // 
            this.btnTrimBlankChars.Label = "去除首尾空白";
            this.btnTrimBlankChars.Name = "btnTrimBlankChars";
            this.btnTrimBlankChars.ScreenTip = "移除首尾所有空白字符";
            this.btnTrimBlankChars.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnTrimBlankChars_Click);
            // 
            // btnRemoveBlankChars
            // 
            this.btnRemoveBlankChars.Label = "去除空白字符";
            this.btnRemoveBlankChars.Name = "btnRemoveBlankChars";
            this.btnRemoveBlankChars.ScreenTip = "去除所有空白字符";
            this.btnRemoveBlankChars.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnRemoveBlankChars_Click);
            // 
            // btnReplaceBlankChars
            // 
            this.btnReplaceBlankChars.Label = "合并空白字符";
            this.btnReplaceBlankChars.Name = "btnReplaceBlankChars";
            this.btnReplaceBlankChars.ScreenTip = "去除首尾空白字符，并将中间连续多个空白字符合并为一个半角空格";
            this.btnReplaceBlankChars.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnReplaceBlankChars_Click);
            // 
            // gpSimpleStatistics
            // 
            this.gpSimpleStatistics.Items.Add(this.btnAggregateByFeild);
            this.gpSimpleStatistics.Items.Add(this.btnAutoAggregateByFeild);
            this.gpSimpleStatistics.Label = "简单统计";
            this.gpSimpleStatistics.Name = "gpSimpleStatistics";
            // 
            // btnAggregateByFeild
            // 
            this.btnAggregateByFeild.Image = global::LunarExcelAddIn.Properties.Resources.application_view_list;
            this.btnAggregateByFeild.Label = "机械字段汇总";
            this.btnAggregateByFeild.Name = "btnAggregateByFeild";
            this.btnAggregateByFeild.ScreenTip = "须手动排序，否则不会考虑值不连续的问题。";
            this.btnAggregateByFeild.ShowImage = true;
            this.btnAggregateByFeild.SuperTip = "按指定字段的不同取值进行分类汇总。会自动进行全角数字转换、文本数字转换。";
            this.btnAggregateByFeild.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAggregateByFeild_Click);
            // 
            // btnAutoAggregateByFeild
            // 
            this.btnAutoAggregateByFeild.Image = global::LunarExcelAddIn.Properties.Resources.application_form_edit;
            this.btnAutoAggregateByFeild.Label = "自动字段汇总";
            this.btnAutoAggregateByFeild.Name = "btnAutoAggregateByFeild";
            this.btnAutoAggregateByFeild.ScreenTip = "汇总前会自动格式化、合并重复。";
            this.btnAutoAggregateByFeild.ShowImage = true;
            this.btnAutoAggregateByFeild.SuperTip = "按指定字段的不同取值进行分类汇总。会自动进行全角数字转换、文本数字转换。";
            this.btnAutoAggregateByFeild.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAutoAggregateByFeild_Click);
            // 
            // gpFormat
            // 
            this.gpFormat.Items.Add(this.btnAddAllBorders);
            this.gpFormat.Items.Add(this.btnClearAllBorders);
            this.gpFormat.Items.Add(this.btnSetLeftAndRightBorder);
            this.gpFormat.Items.Add(this.btnOnlyTopAndBottomBorder);
            this.gpFormat.KeyTip = "F";
            this.gpFormat.Label = "格式";
            this.gpFormat.Name = "gpFormat";
            // 
            // btnAddAllBorders
            // 
            this.btnAddAllBorders.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnAddAllBorders.Image = global::LunarExcelAddIn.Properties.Resources.AllBorders;
            this.btnAddAllBorders.KeyTip = "A";
            this.btnAddAllBorders.Label = "全部框线";
            this.btnAddAllBorders.Name = "btnAddAllBorders";
            this.btnAddAllBorders.ScreenTip = "为选定区域添加所有框线";
            this.btnAddAllBorders.ShowImage = true;
            this.btnAddAllBorders.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAddAllBorders_Click);
            // 
            // btnClearAllBorders
            // 
            this.btnClearAllBorders.Image = global::LunarExcelAddIn.Properties.Resources.NoBorders;
            this.btnClearAllBorders.KeyTip = "C";
            this.btnClearAllBorders.Label = "取消所有框线";
            this.btnClearAllBorders.Name = "btnClearAllBorders";
            this.btnClearAllBorders.ScreenTip = "取消选定区域所有框线";
            this.btnClearAllBorders.ShowImage = true;
            this.btnClearAllBorders.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnClearAllBorders_Click);
            // 
            // btnSetLeftAndRightBorder
            // 
            this.btnSetLeftAndRightBorder.Image = global::LunarExcelAddIn.Properties.Resources.OnlyLeftAndRightBorders;
            this.btnSetLeftAndRightBorder.KeyTip = "S";
            this.btnSetLeftAndRightBorder.Label = "仅两侧框线";
            this.btnSetLeftAndRightBorder.Name = "btnSetLeftAndRightBorder";
            this.btnSetLeftAndRightBorder.ScreenTip = "用于在视觉上区分两块区域";
            this.btnSetLeftAndRightBorder.ShowImage = true;
            this.btnSetLeftAndRightBorder.SuperTip = "这种视觉划分通常是利用空行来实现的。";
            this.btnSetLeftAndRightBorder.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSetLeftAndRightBorder_Click);
            // 
            // btnOnlyTopAndBottomBorder
            // 
            this.btnOnlyTopAndBottomBorder.Image = global::LunarExcelAddIn.Properties.Resources.OnlyTopAndBottomBorders;
            this.btnOnlyTopAndBottomBorder.KeyTip = "T";
            this.btnOnlyTopAndBottomBorder.Label = "仅上下框线";
            this.btnOnlyTopAndBottomBorder.Name = "btnOnlyTopAndBottomBorder";
            this.btnOnlyTopAndBottomBorder.ScreenTip = "用于在视觉上区分两块区域";
            this.btnOnlyTopAndBottomBorder.ShowImage = true;
            this.btnOnlyTopAndBottomBorder.SuperTip = "这种视觉划分通常是利用空行来实现的。";
            this.btnOnlyTopAndBottomBorder.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnOnlyTopAndBottomBorder_Click);
            // 
            // gpPageBreak
            // 
            this.gpPageBreak.Items.Add(this.btnAddPageCutter);
            this.gpPageBreak.Items.Add(this.btnResetPageBreaks);
            this.gpPageBreak.Label = "分页符";
            this.gpPageBreak.Name = "gpPageBreak";
            // 
            // btnAddPageCutter
            // 
            this.btnAddPageCutter.Description = "按选中列中指定的单元格值添加分页符。";
            this.btnAddPageCutter.Image = global::LunarExcelAddIn.Properties.Resources.page_3sides;
            this.btnAddPageCutter.KeyTip = "H";
            this.btnAddPageCutter.Label = "插入水平分页符";
            this.btnAddPageCutter.Name = "btnAddPageCutter";
            this.btnAddPageCutter.ShowImage = true;
            this.btnAddPageCutter.SuperTip = "按选定列中指定单元格值添加分页符。";
            this.btnAddPageCutter.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAddPageCutter_Click);
            // 
            // btnResetPageBreaks
            // 
            this.btnResetPageBreaks.Image = global::LunarExcelAddIn.Properties.Resources.run_rebuild;
            this.btnResetPageBreaks.KeyTip = "R";
            this.btnResetPageBreaks.Label = "重置所有分页符";
            this.btnResetPageBreaks.Name = "btnResetPageBreaks";
            this.btnResetPageBreaks.ShowImage = true;
            this.btnResetPageBreaks.SuperTip = "仅限当前活动工作表。";
            this.btnResetPageBreaks.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnResetPageBreaks_Click);
            // 
            // gpFileAndFolder
            // 
            this.gpFileAndFolder.Items.Add(this.btnSplitByField);
            this.gpFileAndFolder.Items.Add(this.btnCreateDirectoies);
            this.gpFileAndFolder.Items.Add(this.btnTags);
            this.gpFileAndFolder.Items.Add(this.btnMailCop);
            this.gpFileAndFolder.Label = "文件与目录";
            this.gpFileAndFolder.Name = "gpFileAndFolder";
            // 
            // btnSplitByField
            // 
            this.btnSplitByField.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnSplitByField.Image = global::LunarExcelAddIn.Properties.Resources.page_white_stack;
            this.btnSplitByField.Label = "拆分工作表";
            this.btnSplitByField.Name = "btnSplitByField";
            this.btnSplitByField.ShowImage = true;
            this.btnSplitByField.SuperTip = "将当前工作表按指定字段中不同值分别拆分为不同表（或不同文件）。";
            this.btnSplitByField.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSplitByField_Click);
            // 
            // btnCreateDirectoies
            // 
            this.btnCreateDirectoies.Description = "根据选定的单元格中的数据在指定路径下批量创建目录。";
            this.btnCreateDirectoies.Image = global::LunarExcelAddIn.Properties.Resources.folder_add;
            this.btnCreateDirectoies.Label = "批量创建目录";
            this.btnCreateDirectoies.Name = "btnCreateDirectoies";
            this.btnCreateDirectoies.ShowImage = true;
            this.btnCreateDirectoies.SuperTip = "注意：这些单元格里不能包含不能用作文件名（或目录名）的特殊字符[/\\|\"*?:<>]。";
            this.btnCreateDirectoies.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnCreateDirectoies_Click);
            // 
            // btnTags
            // 
            this.btnTags.Image = global::LunarExcelAddIn.Properties.Resources.tag;
            this.btnTags.Label = "标签生成器";
            this.btnTags.Name = "btnTags";
            this.btnTags.ScreenTip = "批量向默认目录导出填充模板得到的标签图。";
            this.btnTags.ShowImage = true;
            this.btnTags.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnTags_Click);
            // 
            // btnMailCop
            // 
            this.btnMailCop.Image = global::LunarExcelAddIn.Properties.Resources.mail1;
            this.btnMailCop.Label = "邮件合并";
            this.btnMailCop.Name = "btnMailCop";
            this.btnMailCop.ShowImage = true;
            this.btnMailCop.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnMailCop_Click);
            // 
            // gpOthers
            // 
            this.gpOthers.Items.Add(this.btn阅卷);
            this.gpOthers.Items.Add(this.button1);
            this.gpOthers.Label = "其它";
            this.gpOthers.Name = "gpOthers";
            this.gpOthers.Visible = false;
            // 
            // btn阅卷
            // 
            this.btn阅卷.Label = "阅卷";
            this.btn阅卷.Name = "btn阅卷";
            this.btn阅卷.SuperTip = "要先选中所有答案，其中应包括参考答案。";
            this.btn阅卷.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btn阅卷_Click);
            // 
            // button1
            // 
            this.button1.Label = "";
            this.button1.Name = "button1";
            // 
            // gpAbout
            // 
            this.gpAbout.Items.Add(this.lblWarning);
            this.gpAbout.Items.Add(this.label2);
            this.gpAbout.Items.Add(this.label1);
            this.gpAbout.KeyTip = "A";
            this.gpAbout.Label = "◆警告◆";
            this.gpAbout.Name = "gpAbout";
            // 
            // lblWarning
            // 
            this.lblWarning.Label = "使用这些功能会导致撤销列表被清空！！！";
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.ScreenTip = "VBA通常也是如此";
            this.lblWarning.SuperTip = "使用 VBA 或 VSTO 是直接操作内存中的 Excel 文件而不是记录用户操作，所以会导致“撤销/重做”列表被清空。";
            // 
            // label2
            // 
            this.label2.Label = "也就是之前所有操作都无法撤销！！！";
            this.label2.Name = "label2";
            // 
            // label1
            // 
            this.label1.Label = "所以使用前尽可能先保存文件！！！";
            this.label1.Name = "label1";
            // 
            // LunarRibbon
            // 
            this.Name = "LunarRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Tabs.Add(this.LunarAddIn);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.LunarRibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.LunarAddIn.ResumeLayout(false);
            this.LunarAddIn.PerformLayout();
            this.gpSelTools.ResumeLayout(false);
            this.gpSelTools.PerformLayout();
            this.gpValidateTools.ResumeLayout(false);
            this.gpValidateTools.PerformLayout();
            this.gpRowAndColumn.ResumeLayout(false);
            this.gpRowAndColumn.PerformLayout();
            this.gpFormatCells.ResumeLayout(false);
            this.gpFormatCells.PerformLayout();
            this.gpSimpleStatistics.ResumeLayout(false);
            this.gpSimpleStatistics.PerformLayout();
            this.gpFormat.ResumeLayout(false);
            this.gpFormat.PerformLayout();
            this.gpPageBreak.ResumeLayout(false);
            this.gpPageBreak.PerformLayout();
            this.gpFileAndFolder.ResumeLayout(false);
            this.gpFileAndFolder.PerformLayout();
            this.gpOthers.ResumeLayout(false);
            this.gpOthers.PerformLayout();
            this.gpAbout.ResumeLayout(false);
            this.gpAbout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab LunarAddIn;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpSelTools;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSelectEmptyCells;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAddComments;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpValidateTools;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnIdCardNumberValidate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnBankCardNumberValidate;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpRowAndColumn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSetBackgroundBetweenLines;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSetBackgroundBetweenColumns;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpFormat;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAddAllBorders;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnClearAllBorders;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSetLeftAndRightBorder;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnOnlyTopAndBottomBorder;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpAbout;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblWarning;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label2;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnFind;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSetAreaBackColor;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpOthers;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnCreateDirectoies;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn阅卷;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnMailCop;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnFindRepeatRecords;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpFileAndFolder;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSplitByField;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnValidateWithRegex;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpSimpleStatistics;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAggregateByFeild;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAutoAggregateByFeild;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSetBackgroundByFirstChar;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpFormatCells;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnTrimBlankChars;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnRemoveBlankChars;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnReplaceBlankChars;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnTags;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAddPageCutter;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup gpPageBreak;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnResetPageBreaks;
    }

    partial class ThisRibbonCollection
    {
        internal LunarRibbon LunarRibbon
        {
            get { return this.GetRibbon<LunarRibbon>(); }
        }
    }
}
