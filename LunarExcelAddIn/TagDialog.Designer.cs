﻿namespace LunarExcelAddIn
{
    partial class TagDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gpDataSource = new System.Windows.Forms.GroupBox();
            this.cmbDataSourceSheet = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnFstRecord = new System.Windows.Forms.Button();
            this.btnPreviewRecord = new System.Windows.Forms.Button();
            this.btnNextRecord = new System.Windows.Forms.Button();
            this.btnLastRecord = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.gpTemplate = new System.Windows.Forms.GroupBox();
            this.cmbTemplateSheet = new System.Windows.Forms.ComboBox();
            this.btnPrintTemplate = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tbxSaveImagesFolderPath = new System.Windows.Forms.TextBox();
            this.btnSetImagesSaveFolderPath = new System.Windows.Forms.Button();
            this.btnSaveAsImages = new System.Windows.Forms.Button();
            this.tbxSetExportFileNameRule = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numFrom = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numTo = new System.Windows.Forms.NumericUpDown();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.tbxSubFoldersRegex = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.btnMoveJpgFilesToSubFolders = new System.Windows.Forms.Button();
            this.btnSaveAsPDF = new System.Windows.Forms.Button();
            this.gpDataSource.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.gpTemplate.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTo)).BeginInit();
            this.SuspendLayout();
            // 
            // gpDataSource
            // 
            this.gpDataSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpDataSource.Controls.Add(this.cmbDataSourceSheet);
            this.gpDataSource.Location = new System.Drawing.Point(16, 106);
            this.gpDataSource.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpDataSource.Name = "gpDataSource";
            this.gpDataSource.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpDataSource.Size = new System.Drawing.Size(408, 81);
            this.gpDataSource.TabIndex = 0;
            this.gpDataSource.TabStop = false;
            this.gpDataSource.Text = "请指定源数据表：";
            // 
            // cmbDataSourceSheet
            // 
            this.cmbDataSourceSheet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDataSourceSheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDataSourceSheet.FormattingEnabled = true;
            this.cmbDataSourceSheet.Location = new System.Drawing.Point(9, 34);
            this.cmbDataSourceSheet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbDataSourceSheet.Name = "cmbDataSourceSheet";
            this.cmbDataSourceSheet.Size = new System.Drawing.Size(391, 28);
            this.cmbDataSourceSheet.Sorted = true;
            this.cmbDataSourceSheet.TabIndex = 0;
            this.cmbDataSourceSheet.DropDown += new System.EventHandler(this.cmbDataSourceSheet_DropDown);
            this.cmbDataSourceSheet.SelectedIndexChanged += new System.EventHandler(this.cmbDataSourceSheet_SelectedIndexChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Controls.Add(this.btnFstRecord, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPreviewRecord, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNextRecord, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnLastRecord, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown1, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(25, 195);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(306, 53);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // btnFstRecord
            // 
            this.btnFstRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFstRecord.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnFstRecord.Location = new System.Drawing.Point(3, 4);
            this.btnFstRecord.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFstRecord.Name = "btnFstRecord";
            this.btnFstRecord.Size = new System.Drawing.Size(39, 45);
            this.btnFstRecord.TabIndex = 0;
            this.btnFstRecord.Text = "|<";
            this.btnFstRecord.UseVisualStyleBackColor = true;
            this.btnFstRecord.Click += new System.EventHandler(this.btnFstRecord_Click);
            // 
            // btnPreviewRecord
            // 
            this.btnPreviewRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPreviewRecord.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPreviewRecord.Location = new System.Drawing.Point(48, 4);
            this.btnPreviewRecord.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPreviewRecord.Name = "btnPreviewRecord";
            this.btnPreviewRecord.Size = new System.Drawing.Size(39, 45);
            this.btnPreviewRecord.TabIndex = 1;
            this.btnPreviewRecord.Text = "<";
            this.btnPreviewRecord.UseVisualStyleBackColor = true;
            this.btnPreviewRecord.Click += new System.EventHandler(this.btnPreviewRecord_Click);
            // 
            // btnNextRecord
            // 
            this.btnNextRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextRecord.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNextRecord.Location = new System.Drawing.Point(215, 4);
            this.btnNextRecord.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNextRecord.Name = "btnNextRecord";
            this.btnNextRecord.Size = new System.Drawing.Size(39, 45);
            this.btnNextRecord.TabIndex = 3;
            this.btnNextRecord.Text = ">";
            this.btnNextRecord.UseVisualStyleBackColor = true;
            this.btnNextRecord.Click += new System.EventHandler(this.btnNextRecord_Click);
            // 
            // btnLastRecord
            // 
            this.btnLastRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLastRecord.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLastRecord.Location = new System.Drawing.Point(260, 4);
            this.btnLastRecord.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLastRecord.Name = "btnLastRecord";
            this.btnLastRecord.Size = new System.Drawing.Size(43, 45);
            this.btnLastRecord.TabIndex = 4;
            this.btnLastRecord.Text = ">|";
            this.btnLastRecord.UseVisualStyleBackColor = true;
            this.btnLastRecord.Click += new System.EventHandler(this.btnLastRecord_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown1.Location = new System.Drawing.Point(102, 13);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(98, 26);
            this.numericUpDown1.TabIndex = 2;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numericUpDown1_KeyDown);
            // 
            // gpTemplate
            // 
            this.gpTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpTemplate.Controls.Add(this.cmbTemplateSheet);
            this.gpTemplate.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gpTemplate.Location = new System.Drawing.Point(16, 17);
            this.gpTemplate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpTemplate.Name = "gpTemplate";
            this.gpTemplate.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpTemplate.Size = new System.Drawing.Size(408, 81);
            this.gpTemplate.TabIndex = 1;
            this.gpTemplate.TabStop = false;
            this.gpTemplate.Text = "请指定数据模板表：";
            // 
            // cmbTemplateSheet
            // 
            this.cmbTemplateSheet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTemplateSheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTemplateSheet.FormattingEnabled = true;
            this.cmbTemplateSheet.Location = new System.Drawing.Point(9, 34);
            this.cmbTemplateSheet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbTemplateSheet.Name = "cmbTemplateSheet";
            this.cmbTemplateSheet.Size = new System.Drawing.Size(391, 28);
            this.cmbTemplateSheet.Sorted = true;
            this.cmbTemplateSheet.TabIndex = 0;
            this.cmbTemplateSheet.DropDown += new System.EventHandler(this.cmbTemplateSheet_DropDown);
            this.cmbTemplateSheet.SelectedIndexChanged += new System.EventHandler(this.cmbTemplateSheet_SelectedIndexChanged);
            // 
            // btnPrintTemplate
            // 
            this.btnPrintTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintTemplate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrintTemplate.Location = new System.Drawing.Point(338, 205);
            this.btnPrintTemplate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPrintTemplate.Name = "btnPrintTemplate";
            this.btnPrintTemplate.Size = new System.Drawing.Size(79, 32);
            this.btnPrintTemplate.TabIndex = 2;
            this.btnPrintTemplate.Text = "打印(&P)";
            this.btnPrintTemplate.UseVisualStyleBackColor = true;
            this.btnPrintTemplate.Click += new System.EventHandler(this.btnPrintTemplate_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 665);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 18, 0);
            this.statusStrip1.Size = new System.Drawing.Size(437, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipTitle = "将当前记录填充到模板表中并保存为 Excel 文件。";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox1.Location = new System.Drawing.Point(0, 552);
            this.textBox1.Margin = new System.Windows.Forms.Padding(11, 14, 11, 14);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(437, 113);
            this.textBox1.TabIndex = 7;
            this.textBox1.Text = "　　以数据源表第一个有效行为字段名！在模板表上以这些字段名命名单元格。\r\n　　注意：\r\n　　① 单元格命名不要用数字开头，否则要注意自动添加的 _ 字符；\r\n　　" +
    "② 工作簿中如果有多个模板时，要注意单元格命名不能重复。";
            // 
            // tbxSaveImagesFolderPath
            // 
            this.tbxSaveImagesFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxSaveImagesFolderPath.Location = new System.Drawing.Point(27, 338);
            this.tbxSaveImagesFolderPath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxSaveImagesFolderPath.Multiline = true;
            this.tbxSaveImagesFolderPath.Name = "tbxSaveImagesFolderPath";
            this.tbxSaveImagesFolderPath.ReadOnly = true;
            this.tbxSaveImagesFolderPath.Size = new System.Drawing.Size(301, 31);
            this.tbxSaveImagesFolderPath.TabIndex = 8;
            // 
            // btnSetImagesSaveFolderPath
            // 
            this.btnSetImagesSaveFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetImagesSaveFolderPath.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSetImagesSaveFolderPath.Location = new System.Drawing.Point(334, 338);
            this.btnSetImagesSaveFolderPath.Margin = new System.Windows.Forms.Padding(0);
            this.btnSetImagesSaveFolderPath.Name = "btnSetImagesSaveFolderPath";
            this.btnSetImagesSaveFolderPath.Size = new System.Drawing.Size(82, 32);
            this.btnSetImagesSaveFolderPath.TabIndex = 9;
            this.btnSetImagesSaveFolderPath.Text = "定位(&D)";
            this.btnSetImagesSaveFolderPath.UseVisualStyleBackColor = true;
            this.btnSetImagesSaveFolderPath.Click += new System.EventHandler(this.btnSetImagesSaveFolderPath_Click);
            // 
            // btnSaveAsImages
            // 
            this.btnSaveAsImages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAsImages.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSaveAsImages.Location = new System.Drawing.Point(309, 401);
            this.btnSaveAsImages.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveAsImages.Name = "btnSaveAsImages";
            this.btnSaveAsImages.Size = new System.Drawing.Size(108, 32);
            this.btnSaveAsImages.TabIndex = 10;
            this.btnSaveAsImages.Text = "导出图像(&I)";
            this.btnSaveAsImages.UseVisualStyleBackColor = true;
            this.btnSaveAsImages.Click += new System.EventHandler(this.btnSaveAsImages_Click);
            // 
            // tbxSetExportFileNameRule
            // 
            this.tbxSetExportFileNameRule.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxSetExportFileNameRule.Location = new System.Drawing.Point(35, 420);
            this.tbxSetExportFileNameRule.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxSetExportFileNameRule.Name = "tbxSetExportFileNameRule";
            this.tbxSetExportFileNameRule.Size = new System.Drawing.Size(257, 26);
            this.tbxSetExportFileNameRule.TabIndex = 8;
            this.tbxSetExportFileNameRule.Text = "{序号}-{所在班级}-{姓名}-{身份证号}";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 387);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "导出的图像文件命名规则：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 301);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(163, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "设置图像文件导出目录：";
            // 
            // numFrom
            // 
            this.numFrom.Location = new System.Drawing.Point(77, 261);
            this.numFrom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.numFrom.Name = "numFrom";
            this.numFrom.Size = new System.Drawing.Size(117, 26);
            this.numFrom.TabIndex = 13;
            this.numFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 263);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "从：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(224, 263);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 20);
            this.label4.TabIndex = 14;
            this.label4.Text = "到：";
            // 
            // numTo
            // 
            this.numTo.Location = new System.Drawing.Point(285, 261);
            this.numTo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.numTo.Name = "numTo";
            this.numTo.Size = new System.Drawing.Size(129, 26);
            this.numTo.TabIndex = 13;
            this.numTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxSubFoldersRegex
            // 
            this.tbxSubFoldersRegex.Location = new System.Drawing.Point(37, 509);
            this.tbxSubFoldersRegex.Name = "tbxSubFoldersRegex";
            this.tbxSubFoldersRegex.Size = new System.Drawing.Size(291, 26);
            this.tbxSubFoldersRegex.TabIndex = 15;
            this.tbxSubFoldersRegex.Text = "高[一二三]\\(\\d{1,2}\\)";
            this.toolTip3.SetToolTip(this.tbxSubFoldersRegex, "1. 用指定的正则表达式对导出的所有图像文件或PDF文件名称进行匹配。\r\n2. 这些匹配出来的文本片段进行去重。\r\n3. 按照去重后的列表创建对应的子目录。\r\n4" +
        ". 将符合某一列表项的目录移动到对应的子目录中。");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 479);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(289, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "将导出文件整理到子目录使用的正则表达式：";
            // 
            // btnMoveJpgFilesToSubFolders
            // 
            this.btnMoveJpgFilesToSubFolders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveJpgFilesToSubFolders.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnMoveJpgFilesToSubFolders.Location = new System.Drawing.Point(334, 505);
            this.btnMoveJpgFilesToSubFolders.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMoveJpgFilesToSubFolders.Name = "btnMoveJpgFilesToSubFolders";
            this.btnMoveJpgFilesToSubFolders.Size = new System.Drawing.Size(85, 32);
            this.btnMoveJpgFilesToSubFolders.TabIndex = 10;
            this.btnMoveJpgFilesToSubFolders.Text = "移动(&M)";
            this.btnMoveJpgFilesToSubFolders.UseVisualStyleBackColor = true;
            this.btnMoveJpgFilesToSubFolders.Click += new System.EventHandler(this.btnMoveJpgOrPdfFilesToSubFolders_Click);
            // 
            // btnSaveAsPDF
            // 
            this.btnSaveAsPDF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAsPDF.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSaveAsPDF.Location = new System.Drawing.Point(308, 438);
            this.btnSaveAsPDF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveAsPDF.Name = "btnSaveAsPDF";
            this.btnSaveAsPDF.Size = new System.Drawing.Size(109, 32);
            this.btnSaveAsPDF.TabIndex = 10;
            this.btnSaveAsPDF.Text = "导出PDF(&P)";
            this.btnSaveAsPDF.UseVisualStyleBackColor = true;
            this.btnSaveAsPDF.Click += new System.EventHandler(this.btnSaveAsPDF_Click);
            // 
            // TagDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 687);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxSubFoldersRegex);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numTo);
            this.Controls.Add(this.numFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMoveJpgFilesToSubFolders);
            this.Controls.Add(this.btnSaveAsPDF);
            this.Controls.Add(this.btnSaveAsImages);
            this.Controls.Add(this.btnSetImagesSaveFolderPath);
            this.Controls.Add(this.tbxSetExportFileNameRule);
            this.Controls.Add(this.tbxSaveImagesFolderPath);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnPrintTemplate);
            this.Controls.Add(this.gpTemplate);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.gpDataSource);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "TagDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "标签图像文件批量生成器";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TagDialog_FormClosing);
            this.gpDataSource.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.gpTemplate.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gpDataSource;
        private System.Windows.Forms.ComboBox cmbDataSourceSheet;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnFstRecord;
        private System.Windows.Forms.Button btnPreviewRecord;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button btnNextRecord;
        private System.Windows.Forms.Button btnLastRecord;
        private System.Windows.Forms.GroupBox gpTemplate;
        private System.Windows.Forms.ComboBox cmbTemplateSheet;
        private System.Windows.Forms.Button btnPrintTemplate;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tbxSaveImagesFolderPath;
        private System.Windows.Forms.Button btnSetImagesSaveFolderPath;
        private System.Windows.Forms.Button btnSaveAsImages;
        private System.Windows.Forms.TextBox tbxSetExportFileNameRule;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numTo;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.TextBox tbxSubFoldersRegex;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.Button btnMoveJpgFilesToSubFolders;
        private System.Windows.Forms.Button btnSaveAsPDF;
    }
}