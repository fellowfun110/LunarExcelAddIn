﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using Excel = Microsoft.Office.Interop.Excel;

namespace LunarExcelAddIn
{
    public partial class TagDialog : Form
    {
        public TagDialog()
        {
            InitializeComponent();
        }

        private void cmbDataSourceSheet_DropDown(object sender, EventArgs e)
        {
            cmbDataSourceSheet.Items.Clear();

            foreach (Excel.Worksheet displayWorksheet in Globals.ThisAddIn.Application.Worksheets)
            {
                cmbDataSourceSheet.Items.Add(displayWorksheet.Name);
            }
        }

        private void cmbTemplateSheet_DropDown(object sender, EventArgs e)
        {
            cmbTemplateSheet.Items.Clear();

            foreach (Excel.Worksheet displayWorksheet in Globals.ThisAddIn.Application.Worksheets)
            {
                cmbTemplateSheet.Items.Add(displayWorksheet.Name);
            }
        }

        private void cmbDataSourceSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();


            if (cmbDataSourceSheet.SelectedIndex >= 0)
            {
                Excel.Worksheet sheetDataSource = Globals.ThisAddIn.Application.Worksheets[cmbDataSourceSheet.SelectedItem as string] as Excel.Worksheet;

                if (sheetDataSource != null)
                {
                    var usedrange = sheetDataSource.UsedRange;
                    if (usedrange != null && usedrange.Rows.Count > 0)
                    {
                        if (usedrange.Rows.Count > 1)
                        {
                            numFrom.Minimum = numTo.Minimum = 1;
                            numFrom.Maximum = numTo.Maximum =
                                numericUpDown1.Maximum = usedrange.Rows.Count - 1;
                            numTo.Value = numTo.Maximum;
                        }
                    }
                }
            }
        }

        private void cmbTemplateSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        /// <summary>
        /// 载入数据填充到模板中。
        /// </summary>
        private bool LoadData(int rowIndex = 1, List<FileNameField> fileNameFields = null)
        {
            if (cmbDataSourceSheet.SelectedIndex < 0) return false;

            if (cmbTemplateSheet.SelectedIndex < 0) return false;

            if (cmbTemplateSheet.SelectedItem.ToString() == cmbDataSourceSheet.SelectedItem.ToString())
            {
                MessageBox.Show("数据源表和模板表不能相同！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            Excel.Worksheet sheetTemplate = Globals.ThisAddIn.Application.Worksheets[cmbTemplateSheet.SelectedItem as string] as Excel.Worksheet;
            Excel.Worksheet sheetDataSource = Globals.ThisAddIn.Application.Worksheets[cmbDataSourceSheet.SelectedItem as string] as Excel.Worksheet;

            var usedRange = sheetDataSource.UsedRange;

            if (usedRange.Rows.Count <= 1 || usedRange.Columns.Count <= 0)
            {
                MessageBox.Show("数据源表中没有可用数据！\r\n\r\n注意：数据源表中第一行应是字段名；这些字段名应在模板中用于对单元格进行命名。", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (rowIndex < 1 || rowIndex >= usedRange.Rows.Count)
            {
                MessageBox.Show("行索引越界！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            // 初始化导出路径
            var folderPath = Globals.ThisAddIn.Application.ActiveWorkbook.Path;
            if (folderPath.EndsWith("\\") == false)
                folderPath += "\\";

            folderPath += sheetDataSource.Name + "-导出图集\\";
            DirectoryInfo fdi;
            try
            {
                fdi = new DirectoryInfo(folderPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("注意：当前活动表的名称中不能带特殊字符（\\/:*?\"<>|）。\r\n错误消息如下：\r\n" + ex.Message);
                return false;
            }

            tbxSaveImagesFolderPath.Text = fdi.FullName;
            toolTip2.SetToolTip(tbxSaveImagesFolderPath, fdi.FullName);

            numericUpDown1.Value = rowIndex;
            var rowFst = Globals.ThisAddIn.Application.Intersect(usedRange, (usedRange[1, 1] as Excel.Range).EntireRow);
            var row = Globals.ThisAddIn.Application.Intersect(usedRange, (usedRange[rowIndex + 1, 1] as Excel.Range).EntireRow);

            int i = 1;
            int validateFieldIndex = 1;
            int blank_count = 0;
            foreach (Excel.Range rng in rowFst)
            {
                if (rng.Value == null) { i++; continue; }
                if (FindName(rng.Value.ToString()) == false) { i++; continue; }

                Excel.Range destRng = sheetTemplate.Range[rng.Value.ToString()];
                if (destRng != null)
                {
                    if (i <= row.Count)
                    {

                        destRng.Value = (usedRange[rowIndex + 1, i] as Excel.Range).Value;

                        // 取出文件名的片段。
                        if (fileNameFields != null)
                        {
                            foreach (var fileNameField in fileNameFields)
                            {
                                var fieldName = rng.Value.ToString().Trim();
                                if (fieldName.ToLower() == fileNameField.FieldName.Trim().ToLower())
                                {
                                    fileNameField.FieldValue = destRng.Value.ToString();
                                }
                            }
                        }

                        if (destRng.Value == null || string.IsNullOrEmpty(destRng.Value.ToString()))
                        {
                            blank_count++;
                        }
                    }
                    else
                    {
                        destRng.Value = "";
                        blank_count++;
                    }
                }
                i++;
                validateFieldIndex++;
            }

            if (blank_count >= validateFieldIndex - 1)
            {
                MessageBox.Show("数据源表中指定行记录无可填充内容！请检查是不是数据源表中是否有内容，或者是否选择数据源表和数据模板时设置反了。", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return true;
        }

        private bool LoadDataFromWorkBook(Excel.Workbook srcWorkbook, int rowIndex = 1)
        {
            if (srcWorkbook == null) return false;

            if (cmbDataSourceSheet.SelectedIndex < 0)
            {
                //太啰嗦
                //MessageBox.Show("请先指定用作模板的工作表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (cmbTemplateSheet.SelectedIndex < 0)
            {
                //太啰嗦
                //MessageBox.Show("请先指定用作数据源的工作表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (cmbTemplateSheet.SelectedItem.ToString() == cmbDataSourceSheet.SelectedItem.ToString())
            {
                MessageBox.Show("数据源表和模板表不能相同！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            Excel.Worksheet sheetTemplate = srcWorkbook.Worksheets[cmbTemplateSheet.SelectedItem as string] as Excel.Worksheet;
            Excel.Worksheet sheetDataSource = srcWorkbook.Worksheets[cmbDataSourceSheet.SelectedItem as string] as Excel.Worksheet;

            var usedRange = sheetDataSource.UsedRange;

            if (usedRange.Rows.Count <= 1 || usedRange.Columns.Count <= 0)
            {
                MessageBox.Show("数据源表中没有可用数据！\r\n\r\n注意：数据源表中第一行应是字段名；这些字段名应在模板中用于对单元格进行命名。", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (rowIndex < 1 || rowIndex >= usedRange.Rows.Count)
            {
                // MessageBox.Show("行索引越界！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            var rowFst = Globals.ThisAddIn.Application.Intersect(usedRange, (usedRange[1, 1] as Excel.Range).EntireRow);
            var row = Globals.ThisAddIn.Application.Intersect(usedRange, (usedRange[rowIndex + 1, 1] as Excel.Range).EntireRow);

            int i = 1;
            int validateFieldIndex = 1;
            int blank_count = 0;
            foreach (Excel.Range rng in rowFst)
            {
                if (rng.Value == null) { i++; continue; }
                if (FindNameFromWorkbook(srcWorkbook, rng.Value.ToString()) == false) { i++; continue; }

                Excel.Range destRng = sheetTemplate.Range[rng.Value.ToString()];
                if (destRng != null)
                {
                    if (i <= row.Count)
                    {
                        destRng.Value = (usedRange[rowIndex + 1, i] as Excel.Range).Value;
                        if (destRng.Value == null || string.IsNullOrEmpty(destRng.Value.ToString()))
                        {
                            blank_count++;
                        }
                    }
                    else
                    {
                        destRng.Value = "";
                        blank_count++;
                    }
                }
                i++;
                validateFieldIndex++;
            }

            if (blank_count >= validateFieldIndex - 1)
            {
                //MessageBox.Show("此行记录无可填充内容！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);              return false;  // 此处与 LoadData() 不同。
            }
            return true;
        }


        private bool FindName(string destname)
        {
            foreach (Excel.Name name in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
            {
                if (name.Name == destname) return true;
            }
            return false;
        }

        private bool FindNameFromWorkbook(Excel.Workbook srcWorkbook, string destname)
        {
            if (srcWorkbook == null) return false;

            foreach (Excel.Name name in srcWorkbook.Names)
            {
                if (name.Name == destname) return true;
            }
            return false;
        }

        private void btnFstRecord_Click(object sender, EventArgs e)
        {
            var oldNum = numericUpDown1.Value;
            numericUpDown1.Value = 1;

            if (LoadData(1) == false)
            {
                numericUpDown1.Value = oldNum;
            }
        }

        private void btnPreviewRecord_Click(object sender, EventArgs e)
        {
            numericUpDown1.Value--;

            if (LoadData((int)numericUpDown1.Value) == false)
            {
                numericUpDown1.Value++;
            }
        }

        private void btnNextRecord_Click(object sender, EventArgs e)
        {
            numericUpDown1.Value++;

            if (LoadData((int)numericUpDown1.Value) == false)
            {
                numericUpDown1.Value--;
            }
        }

        private void btnLastRecord_Click(object sender, EventArgs e)
        {
            var oldNum = numericUpDown1.Value;
            numericUpDown1.Value = numericUpDown1.Maximum;

            if (LoadData((int)numericUpDown1.Value) == false)
            {
                numericUpDown1.Value = oldNum;
            }
        }

        private void btnPrintTemplate_Click(object sender, EventArgs e)
        {
            if (cmbTemplateSheet.SelectedIndex < 0) return;

            Excel.Worksheet sheetTemplate = Globals.ThisAddIn.Application.Worksheets[cmbTemplateSheet.SelectedItem as string] as Excel.Worksheet;
            if (sheetTemplate == null) return;

            sheetTemplate.PrintPreview();
        }

        private void numericUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadData((int)numericUpDown1.Value);
            }
        }

        private void TagDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private string GetLocationCellMark(int i, int columnCount, Range usedRange)
        {
            var usedColCount = usedRange.Columns.Count;
            var usedRowCount = usedRange.Rows.Count;

            int rowIndex;
            int colIndex;
            int tmp = i % columnCount;
            if (tmp == 0)
            {
                rowIndex = (usedRowCount + 1) * ((int)(i / columnCount) - 1) + 1;
                colIndex = (usedColCount + 1) * (columnCount - 1) + 1;
            }
            else
            {
                rowIndex = (usedRowCount + 1) * ((int)(i / columnCount)) + 1;
                colIndex = (usedColCount + 1) * (tmp - 1) + 1;
            }

            return $"{ConvertToAlpha(colIndex)}{rowIndex}";
        }

        private object ConvertToAlpha(int colIndex)
        {
            if (colIndex >= 1 && colIndex <= 26)
            {
                var acode = (int)'a';
                var bcode = acode + colIndex - 1;
                var result_char = (char)bcode;
                return $"{result_char}";
            }

            return "a";
        }

        /// <summary>
        /// [备用]此方法抄自：
        /// https://www.evget.com/article/2009/4/24/10620.html
        /// </summary>
        /// <param name="app"></param>
        /// <param name="pasteRange"></param>
        /// <param name="beginCell"></param>
        /// <param name="fileName"></param>
        public static void CreatAndPaste(Excel.Application app, string pasteRange, string beginCell, string fileName)
        {
            //使用新的Excel进程添加一个新的工作簿
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(Type.Missing);
            Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets[1];

            //粘贴
            ws.get_Range(beginCell, pasteRange).PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteValues,
                Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, Type.Missing, Type.Missing);
            ws.get_Range(beginCell, pasteRange).PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats,
                Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, Type.Missing, Type.Missing);
            ws.get_Range(beginCell, pasteRange).Columns.AutoFit();

            //保存
            wb.SaveCopyAs(fileName);

            //关闭工作簿
            Clipboard.Clear();
            wb.Close(false, Type.Missing, Type.Missing);

        }

        private void btnSaveAsImages_Click(object sender, EventArgs e)
        {
            if (cmbDataSourceSheet.SelectedIndex < 0)
            {
                MessageBox.Show("没找到数据源表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cmbTemplateSheet.SelectedIndex < 0)
            {
                MessageBox.Show("没找到填充模板表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Excel.Worksheet sheetTemplate = Globals.ThisAddIn.Application.Worksheets[cmbTemplateSheet.SelectedItem as string] as Excel.Worksheet;
            if (sheetTemplate == null)
            {
                MessageBox.Show("没找到填充模板表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Excel.Worksheet sheetDataSource = Globals.ThisAddIn.Application.Worksheets[cmbDataSourceSheet.SelectedItem as string] as Excel.Worksheet;
            if (sheetDataSource == null)
            {
                MessageBox.Show("没找到数据源表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                var di = new DirectoryInfo(tbxSaveImagesFolderPath.Text);
                if (di.Exists == false) di.Create();

                var usedrange = sheetDataSource.UsedRange;

                var srcWorkBook = Globals.ThisAddIn.Application.ActiveWorkbook;
                var fileNameFieldsList = FileNameField.Build(tbxSetExportFileNameRule.Text);

                var fromIndex = (int)numFrom.Value;
                var toIndex = (int)numTo.Value;

                if (toIndex < fromIndex)
                {
                    MessageBox.Show("请重设导出记录的起止范围！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var count = 0;

                for (int i = fromIndex; i <= toIndex; i++)
                {
                    if (LoadData(i, fileNameFieldsList))
                    {
                        // 复制到剪贴板
                        sheetTemplate.UsedRange.CopyPicture(XlPictureAppearance.xlScreen, XlCopyPictureFormat.xlBitmap);

                        var shortName = new StringBuilder();
                        foreach (var item in fileNameFieldsList)
                        {
                            shortName.Append(item.FieldValue);
                            shortName.Append("-");
                        }
                        var shortFileName = shortName.ToString();
                        if (shortFileName.EndsWith("-"))
                            shortFileName = shortFileName.TrimEnd(new char[] { '-', });

                        var imgFilePath = tbxSaveImagesFolderPath.Text + shortFileName + ".jpg";

                        if (Clipboard.ContainsData(DataFormats.EnhancedMetafile))
                        {
                            var metafile = Clipboard.GetData(DataFormats.EnhancedMetafile) as System.Drawing.Imaging.Metafile;
                            if (Directory.Exists(tbxSaveImagesFolderPath.Text) == false)
                            {
                                Directory.CreateDirectory(tbxSaveImagesFolderPath.Text);
                            }
                            metafile.Save(imgFilePath);
                        }
                        else if (Clipboard.ContainsData(DataFormats.Bitmap))
                        {
                            Bitmap bitmap = Clipboard.GetData(DataFormats.Bitmap) as Bitmap;
                            if (Directory.Exists(tbxSaveImagesFolderPath.Text) == false)
                            {
                                Directory.CreateDirectory(tbxSaveImagesFolderPath.Text);
                            }
                            bitmap.Save(imgFilePath);
                            count++;
                        }
                    }
                }

                if (tbxSubFoldersRegex.Text.Length <= 0)
                {
                    MessageBox.Show($"已导出 {count} 张图片。", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    var answer = MessageBox.Show($"已导出 {count} 张图片。\r\n\r\n要按照 子目录正则表达式 将导出目录中所有 JPG 图像文件移动到匹配到的对应子目录中吗？", "LunarExcel-AddIn",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (answer != DialogResult.Yes) return;

                    btnMoveJpgOrPdfFilesToSubFolders_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("导出图像文件出错！异常消息如下：\r\n" + ex.Message, "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void btnSetImagesSaveFolderPath_Click(object sender, EventArgs e)
        {
            if (tbxSaveImagesFolderPath.Text.Contains(" "))
            {
                System.Diagnostics.Process.Start("explorer.exe", "\"" + tbxSaveImagesFolderPath.Text + "\"");
            }
            else
            {
                System.Diagnostics.Process.Start("explorer.exe", tbxSaveImagesFolderPath.Text);
            }
        }

        private void btnMoveJpgOrPdfFilesToSubFolders_Click(object sender, EventArgs e)
        {
            //try
            //{
            var di = new DirectoryInfo(tbxSaveImagesFolderPath.Text);
            var imagePathList = Directory.GetFiles(di.FullName);

            var regImgFileName = new Regex(tbxSubFoldersRegex.Text);

            var movedCount = 0;
            foreach (var imgFilePath in imagePathList)
            {
                var low = imgFilePath.ToLower();
                if (low.EndsWith(".jpg") == false && low.EndsWith(".pdf") == false) continue;

                var ifi = new FileInfo(imgFilePath);
                var imatch = regImgFileName.Match(ifi.Name);
                if (imatch.Success)
                {
                    string subImageFolderPath;
                    if (ifi.Directory.FullName.EndsWith("\\") == false)
                    {
                        subImageFolderPath = ifi.Directory.FullName + "\\" + imatch.Value;
                    }
                    else
                    {
                        subImageFolderPath = ifi.Directory.FullName + imatch.Value;
                    }

                    if (Directory.Exists(subImageFolderPath) == false)
                    {
                        Directory.CreateDirectory(subImageFolderPath);
                    }

                    if (subImageFolderPath.EndsWith("\\") == false) subImageFolderPath += "\\";

                    ifi.MoveTo(subImageFolderPath + ifi.Name);
                    movedCount++;
                }
            }

            if (movedCount > 0)
            {
                MessageBox.Show($"已按指定的正则表达式分配至对应子目录。", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show($"貌似指定的正则表达式没有匹配到图像（或PDF）文件。您可以观察导出的这些图像（或PDF）文件的名称，然后再修改正则表达式。", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //}
            //catch(Exception ex)
            //{
            //    MessageBox.Show("移动图像文件出错！异常消息如下：\r\n" + ex.Message, "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}
        }

        private void btnSaveAsPDF_Click(object sender, EventArgs e)
        {
            if (cmbDataSourceSheet.SelectedIndex < 0)
            {
                MessageBox.Show("没找到数据源表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cmbTemplateSheet.SelectedIndex < 0)
            {
                MessageBox.Show("没找到填充模板表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Excel.Worksheet sheetTemplate = Globals.ThisAddIn.Application.Worksheets[cmbTemplateSheet.SelectedItem as string] as Excel.Worksheet;
            if (sheetTemplate == null)
            {
                MessageBox.Show("没找到填充模板表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Excel.Worksheet sheetDataSource = Globals.ThisAddIn.Application.Worksheets[cmbDataSourceSheet.SelectedItem as string] as Excel.Worksheet;
            if (sheetDataSource == null)
            {
                MessageBox.Show("没找到数据源表！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                var di = new DirectoryInfo(tbxSaveImagesFolderPath.Text);
                if (di.Exists == false) di.Create();

                var usedrange = sheetDataSource.UsedRange;

                var srcWorkBook = Globals.ThisAddIn.Application.ActiveWorkbook;
                var fileNameFieldsList = FileNameField.Build(tbxSetExportFileNameRule.Text);

                var fromIndex = (int)numFrom.Value;
                var toIndex = (int)numTo.Value;

                if (toIndex < fromIndex)
                {
                    MessageBox.Show("请重设导出记录的起止范围！", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var count = 0;

                for (int i = fromIndex; i <= toIndex; i++)
                {
                    if (LoadData(i, fileNameFieldsList))
                    {
                        var shortName = new StringBuilder();
                        foreach (var item in fileNameFieldsList)
                        {
                            shortName.Append(item.FieldValue);
                            shortName.Append("-");
                        }
                        var shortFileName = shortName.ToString();
                        if (shortFileName.EndsWith("-"))
                            shortFileName = shortFileName.TrimEnd(new char[] { '-', });

                        var pdfFilePath = tbxSaveImagesFolderPath.Text + shortFileName + ".pdf";
                        sheetTemplate.ExportAsFixedFormat(XlFixedFormatType.xlTypePDF, pdfFilePath);

                        count++;
                    }
                }

                if (tbxSubFoldersRegex.Text.Length <= 0)
                {
                    MessageBox.Show($"已导出 {count} 个 PDF 文件。", "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    var answer = MessageBox.Show($"已导出 {count} 个 PDF 文件。\r\n\r\n要按照 子目录正则表达式 将导出目录中所有 PDF 文件移动到匹配到的对应子目录中吗？", "LunarExcel-AddIn",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (answer != DialogResult.Yes) return;

                    btnMoveJpgOrPdfFilesToSubFolders_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("导出 PDF 文件出错！异常消息如下：\r\n" + ex.Message, "LunarExcel-AddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }
    }

    /// <summary>
    /// 用以拼成文件短名。
    /// </summary>
    public class FileNameField
    {
        public string FieldName { get; set; }

        public string FieldValue { get; set; }

        public static List<FileNameField> Build(string text)
        {
            var reg = new Regex(@"(?<=\{).*?(?=\})");
            var matches = reg.Matches(text);
            if (matches.Count <= 0) return null;

            var lst = new List<FileNameField>();
            foreach (Match m in matches)
            {
                lst.Add(new FileNameField()
                {
                    FieldName = m.Value,
                });
            }

            return lst;
        }
    }
}
















