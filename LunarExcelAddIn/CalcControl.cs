﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LunarExcelAddIn
{
    public partial class CalcControl : UserControl
    {
        public CalcControl()
        {
            InitializeComponent();
        }

        private void btnSum_Click(object sender, EventArgs e)
        {
            //lblSum.Text = (num1.Value + num2.Value).ToString();
            lblSum.Text = $"{num1.Value + num2.Value}";
        }
    }
}
