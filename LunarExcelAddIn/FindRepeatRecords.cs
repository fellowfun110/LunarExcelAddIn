﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LunarExcelAddIn
{
    public partial class FindRepeatRecords : Form
    {
        public FindRepeatRecords()
        {
            InitializeComponent();


            //读取所有工作表的名称

            int i = 0;
            int selSheetIndex = 0;
            foreach (Worksheet sht in Globals.ThisAddIn.Application.Sheets)
            {
                i++;
                cmbSheetNames.Items.Add(sht.Name);
                if (Globals.ThisAddIn.Application.ActiveSheet == sht)
                {
                    selSheetIndex = i;
                    sourceSheet = sht;
                }
            }

            cmbSheetNames.SelectedIndex = selSheetIndex - 1;

            FillCellsToListBox();
        }

        private Worksheet sourceSheet = null;

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            FillCellsToListBox();
        }

        private void FillCellsToListBox()
        {
            cklstColumns.Items.Clear();

            if (sourceSheet == null) return;
            if (cmbSheetNames.SelectedIndex < 0) return;
            if (numericUpDown1.Value < sourceSheet.UsedRange.Row) return;

            Range titleCells = sourceSheet.Cells[numericUpDown1.Value, 1];
            titleCells = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, titleCells.EntireRow);

            if (titleCells != null && titleCells.Count > 0)
            {
                foreach (Range cell in titleCells)
                {
                    if (cell == null) continue;//可能存在合并单元格

                    string columnTitleText;
                    if (cell.Value == null) columnTitleText = "[空列标题]";
                    else columnTitleText = cell.Value.ToString();

                    cklstColumns.Items.Add(columnTitleText);
                }
            }
        }

        private void btnFindRepeats_Click(object sender, EventArgs e)
        {
            if (sourceSheet == null)
            {
                MessageBox.Show("当前没有打开可供查询的工作表！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (sourceSheet.UsedRange.Count <= 0)
            {
                MessageBox.Show("选定的表中没有可供查询的数据！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cklstColumns.CheckedItems.Count <= 0)
            {
                MessageBox.Show("请选中至少一列作为查找结果！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //从哪一列开始，用户选择这一列必须准确，否则结果就不全。
            int startRowIndex = (int)numericUpDown1.Value + 2 - sourceSheet.UsedRange.Row;//标题行不算。

            //先取出要出现在结果中的列标题
            List<int> columnIndexes = new List<int>();
            for (int i = 0; i < cklstColumns.Items.Count; i++)
            {
                if (cklstColumns.GetItemChecked(i))
                    columnIndexes.Add(i);
            }

            if (columnIndexes.Count != cklstColumns.CheckedItems.Count)
            {
                MessageBox.Show("发生意外事故！选择的列的索引数目与选中项的个数竟然不一致！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //选不选要查找的列结果都是一样的，但是如果选中了要查找的列，会多一列同样的内容。

            bool updateScreen = Globals.ThisAddIn.Application.ScreenUpdating;
            Globals.ThisAddIn.Application.ScreenUpdating = false;

            // 基本思路：利用 List<Record> 进行多次排序，排序后再比较有无连续值相同。
            //           自动忽略大小写！


            // 取出标题中各字段名
            var records = new RecordList();

            Range titleRange = Globals.ThisAddIn.Application.Intersect(
                sourceSheet.Rows[(int)numericUpDown1.Value],
                sourceSheet.UsedRange);

            records.FieldNames.Add("InnerNumber");  // 内置序号列。
            for (int i = 0; i < columnIndexes.Count; i++)
            {
                Range rng = titleRange[columnIndexes[i] + 1];
                records.FieldNames.Add(rng.Value.ToString());
            }

            // 根据用户选择的各列，取出数据，添加到 List<Record> 中

            Range rngData = Globals.ThisAddIn.Application.Intersect(
                sourceSheet.UsedRange,
                sourceSheet.Range[
                    sourceSheet.Cells[(int)numericUpDown1.Value + 1, sourceSheet.UsedRange.Column],
                    sourceSheet.Cells[sourceSheet.UsedRange.Row + sourceSheet.UsedRange.Rows.Count - 1, sourceSheet.UsedRange.Column + sourceSheet.UsedRange.Columns.Count - 1]
                    ]);

            for (int rowIndex = 1; rowIndex <= rngData.Rows.Count; rowIndex++)
            {
                Range row = rngData.Rows[rowIndex];
                var newRecord = new Record();
                newRecord.Fields.Add(new Field() { Value = rowIndex.ToString(), });     // 内置序列号，用于排序后恢复原有序列。
                for (int colIndex = 1; colIndex <= rngData.Columns.Count; colIndex++)
                {
                    if (columnIndexes.Contains(colIndex - 1) == false) continue;

                    Range cell = row.Cells[1, colIndex];
                    newRecord.Fields.Add(new Field() { Value = cell.Value.ToString(), });
                }
                records.Add(newRecord);
            }

            //Range fstColRange = Globals.ThisAddIn.Application.Intersect(
            //    sourceSheet.Columns[columnIndexes[0] + sourceSheet.UsedRange.Column],
            //    sourceSheet.UsedRange);

            //for (int i = (int)numericUpDown1.Value + 1; i <= (sourceSheet.UsedRange.Row + sourceSheet.UsedRange.Rows.Count -1); i++)  // 自动忽略标题行。
            //{
            //    Range rng = sourceSheet.Cells[i, columnIndexes[0]];
            //    var record = new Record();
            //    record.Fields.Add(new Field() { Value = (i - (int)numericUpDown1.Value).ToString(), });  // 内置序号列，不可能重复。
            //    if (rng.Value == null)
            //    {
            //        record.Fields.Add(new Field() { Value = "", });
            //    }
            //    else
            //    {
            //        record.Fields.Add(new Field() { Value = rng.Value.ToString(), });
            //    }

            //    records.Add(record);
            //}

            //for (int i = 1; i < columnIndexes.Count; i++)
            //{
            //    Range otherColRange = Globals.ThisAddIn.Application.Intersect(
            //    sourceSheet.Columns[columnIndexes[i] + sourceSheet.UsedRange.Column],
            //    sourceSheet.UsedRange);
            //    for (int j = (int)numericUpDown1.Value + 1; j <= otherColRange.Count; j++)
            //    {
            //        var rng = otherColRange[j, 1];
            //        var record = records[j - (int)numericUpDown1.Value - 1];
            //        record.Fields.Add(new Field() { Value = rng.Value.ToString(), });
            //    }
            //}

            for (int i = 1; i < records.FieldNames.Count; i++)   // 第0列是内置序号列，不可能重复——用于最终排序后恢复初始排序。
            {
                records.Sort(new RecordComparer(i));
                //排序是为了方便判断有无重复！
                //判断后重新恢复原有排序——这就是内置序号列的作用！

                if (records.Count <= 0) continue;
                if (records.Count == 1)
                {
                    foreach (var field in records[0].Fields)
                    {
                        field.IsRepeat = false;
                    }
                    continue;
                }

                var preRecord = records[0];
                for (int j = 1; j < records.Count; j++)
                {
                    var thisRecord = records[j];
                    var preVal = preRecord.Fields[i].Value;
                    var thisVal = thisRecord.Fields[i].Value;
                    if (ckxIgnoreCase.Checked == true)
                    {
                        preVal = preVal.ToLower();
                        thisVal = thisVal.ToLower();
                    }

                    if (preVal == thisVal)
                    {
                        preRecord.Fields[i].IsRepeat =
                            thisRecord.Fields[i].IsRepeat = true;
                    }
                    else
                    {
                        thisRecord.Fields[i].IsRepeat = false;
                    }
                    preRecord = thisRecord;
                }
            }

            records.Sort(new RecordComparer(0));  // 恢复初始序列

            //StringBuilder sb = new StringBuilder();
            //foreach (var r in records)
            //{
            //    for (int i = 1; i < r.Fields.Count; i++)
            //    {
            //        Field f = r.Fields[i];
            //        sb.Append(f.Value);
            //        sb.Append("\t");
            //        sb.Append(f.IsRepeat.ToString());
            //        sb.Append("\t");
            //    }
            //    sb.Append("\n");
            //}

            //MessageBox.Show(sb.ToString());

            dataGridView1.Rows.Clear();//清空上次查询出来的数据。
            dataGridView1.ColumnCount = (records.FieldNames.Count - 1) * 2;            //加的是查询条件列；第0列不入 DataGridView
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.Tomato;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font =
                new System.Drawing.Font(dataGridView1.Font, FontStyle.Bold);

            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;

            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            dataGridView1.TopLeftHeaderCell.Value = "No.";

            for (int i = 1; i < records.FieldNames.Count; i++)  // 注意，第0列不加入 DataGridView
            {
                dataGridView1.Columns[i * 2 - 2].Name = records.FieldNames[i];  // 隔一个列标题；FieldNames 就是从1开始。
                dataGridView1.Columns[i * 2 - 1].Name = "是否重复";
            }

            var repeatCellsCount = 0;
            for (int rIndex = 0; rIndex < records.Count; rIndex++)
            {
                var r = records[rIndex];

                string[] newRow = new string[(r.Fields.Count - 1) * 2];
                for (int i = 1; i < r.Fields.Count; i++)
                {
                    newRow[i * 2 - 2] = r.Fields[i].Value;                   // 注意：第0列不入 DataGridView
                    newRow[i * 2 - 1] = r.Fields[i].IsRepeat ? "是" : "-";
                }
                dataGridView1.Rows.Add(newRow);
                var newDataRow = dataGridView1.Rows[dataGridView1.Rows.Count - 1];
                newDataRow.HeaderCell.Value = (rIndex + 1).ToString();
                if (r.RepeatFieldsCount > 0)
                {
                    for (int j = 1; j <= newDataRow.Cells.Count; j++)
                    {
                        DataGridViewCell cell = newDataRow.Cells[j - 1];

                        if (j % 2 == 0 && cell.Value.ToString() == "是")
                        {
                            cell.Style.BackColor = Color.Pink;
                            if (j >= 2)
                            {
                                newDataRow.Cells[j - 2].Style.BackColor = Color.LightGreen;
                            }
                            repeatCellsCount++;
                        }
                    }
                    //dataGridView1.Rows[dataGridView1.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightPink;
                }
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;

            if (repeatCellsCount <= 0)
            {
                MessageBox.Show($"指定区域没有找到重复值。", "LunarExcelAddIn", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                tabControl1.SelectedIndex = 1;

                MessageBox.Show($"找到【{repeatCellsCount}】个重复值，均以粉色背景标记，请仔细观察。", "LunarExcelAddIn", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < cklstColumns.Items.Count; i++)
            {
                cklstColumns.SetItemChecked(i, true);
            }
        }

        private void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < cklstColumns.Items.Count; i++)
            {
                cklstColumns.SetItemChecked(i, false);
            }
        }

        private void btnSwitchSelection_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < cklstColumns.Items.Count; i++)
            {
                cklstColumns.SetItemChecked(i, !cklstColumns.GetItemChecked(i));
            }
        }

        private void btnPasteToNewSheet_Click(object sender, EventArgs e)
        {
            var newSheet = (Worksheet)Globals.ThisAddIn.Application.Sheets.Add(Globals.ThisAddIn.Application.ActiveSheet);

            newSheet.Cells.NumberFormatLocal = "@";

            //VBA Sample Code:
            //    Range("D14").Select
            //    ActiveSheet.PasteSpecial Format:= "HTML", Link:= False, DisplayAsIcon:= _
            //        False, NoHTMLFormatting:= True
            dataGridView1.SelectAll();

            var data = dataGridView1.GetClipboardContent();

            if (data == null)
            {
                MessageBox.Show("没有内容可以粘贴！", "LunarExcelAddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Clipboard.SetDataObject(data);

            Globals.ThisAddIn.Application.ScreenUpdating = false;

            newSheet.Cells[1, 1].Select();
            newSheet.PasteSpecial("HTML", false, false, null, null, null, true);
            //为什么使用这个方法？
            //因为这里是从 DataGridView 向 Excel 粘贴数据，而不是在 Excel 工作表之间粘贴数据。

            //VBA code...
            //With Selection.Interior
            //  .Pattern = xlSolid
            //  .PatternColorIndex = xlAutomatic
            //  .ThemeColor = xlThemeColorDark1
            //  .TintAndShade = -0.149998474074526
            //  .PatternTintAndShade = 0
            //End With

            var startRowIndex = 1;
            // 可能出现单元格内换行的情况，这时候，粘贴的标题部分会变成多行。所以要算清有几行标题。
            startRowIndex += (newSheet.UsedRange.Rows.Count - dataGridView1.RowCount);

            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        private void cmbSheetNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            sourceSheet = Globals.ThisAddIn.Application.Sheets[cmbSheetNames.SelectedIndex + 1];
            var usedColumnCount = sourceSheet.UsedRange.Columns.Count;
            if (usedColumnCount <= 0 || sourceSheet.UsedRange.Count <= 0)
            {
                MessageBox.Show($"表 【{cmbSheetNames.Text}】 中没有可供查询的数据！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            numericUpDown1.Maximum = sourceSheet.UsedRange.Row + sourceSheet.UsedRange.Rows.Count - 1;
            numericUpDown1.Value = sourceSheet.UsedRange.Row;
            //FillCellsToListBox();//没必要
        }
    }

    public class Field
    {
        public string Value { get; set; }
        public bool IsRepeat { get; set; }
    }

    public class Record
    {
        public List<Field> Fields = new List<Field>();

        public int RepeatFieldsCount
        {
            get
            {
                int count = 0;
                foreach (var field in Fields)
                {
                    if (field.IsRepeat)
                        count++;
                }
                return count;
            }
        }

        public string GetFieldValue(int index)
        {
            if (index < 0 || index >= Fields.Count) return null;
            return Fields[index].Value;
        }
    }

    public class RecordList : List<Record>
    {
        public List<string> FieldNames = new List<string>();
    }
}
