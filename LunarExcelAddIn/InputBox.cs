﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LunarExcelAddIn
{
    public partial class InputBox : Form
    {
        public InputBox()
        {
            InitializeComponent();
        }

        public string InputedText { get { return tbxText.Text; } }

        public bool IgnoreBlanks { get { return ckxIgnoreBlanks.Checked; } }

        public bool IgnoreCase { get { return ckxInnoreCase.Checked; } }

        private void btnInsertPageBreaks_Click(object sender, EventArgs e)
        {
            if (ckxAutoResetPageBreaks.Checked)
            {
                btnResetPageBreaks_Click(sender, e);
            }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void btnResetPageBreaks_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.Application.ActiveCell.Worksheet.ResetAllPageBreaks();
        }
    }
}
