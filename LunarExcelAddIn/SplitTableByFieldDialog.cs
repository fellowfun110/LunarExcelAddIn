﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace LunarExcelAddIn
{
    public partial class SplitTableByFieldDialog : Form
    {
        public SplitTableByFieldDialog()
        {
            InitializeComponent();


            //读取所有工作表的名称

            int i = 0;
            int selSheetIndex = 0;
            foreach (Worksheet sht in Globals.ThisAddIn.Application.Sheets)
            {
                i++;
                cmbSheetNames.Items.Add(sht.Name);
                if (Globals.ThisAddIn.Application.ActiveSheet == sht)
                {
                    selSheetIndex = i;
                    sourceSheet = sht;
                }
            }

            cmbSheetNames.SelectedIndex = selSheetIndex - 1;

            FillCellsToListBox();
        }


        private void FillCellsToListBox()
        {
            cklstColumns.Items.Clear();

            if (sourceSheet == null) return;
            if (cmbSheetNames.SelectedIndex < 0) return;
            if (numericUpDown1.Value < sourceSheet.UsedRange.Row) return;

            Range titleCells = sourceSheet.Cells[numericUpDown1.Value, 1];
            titleCells = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, titleCells.EntireRow);

            if (titleCells != null && titleCells.Count > 0)
            {
                foreach (Range cell in titleCells)
                {
                    if (cell == null) continue;//可能存在合并单元格

                    string columnTitleText;
                    if (cell.Value == null) columnTitleText = "[空列标题]";
                    else columnTitleText = cell.Value.ToString();

                    cklstColumns.Items.Add(columnTitleText);
                }
            }
        }


        private void btnSaveRecordsAsFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (sourceSheet == null)
                {
                    MessageBox.Show("源数据表为 null。", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (cklstColumns.CheckedItems.Count != 1)
                {
                    MessageBox.Show("只允许选择一个字段来切分文件。", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (sourceSheet.UsedRange.Rows.Count < 2)
                {
                    MessageBox.Show("表中至少有两行数据，第一行应是标题行。", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var answer = MessageBox.Show("执行此操作之前，必须按基准字段先执行【排序】操作。\r\n\r\n" +
                    "如果出现不连续的同值字段值，很容易因误操作造成【误覆盖】或【生成的文件不完整】。\r\n\r\n" +
                    "你确定要继续吗？",
                    "Lunar Excel Addin", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (answer != DialogResult.Yes)
                {
                    return;
                }

                // 基本思路：
                //   1. 取标题栏
                //   2. 创建新表
                //   3. 在新表中填充同一个字段值的所有列
                //   4. 保存新表（用字段值）
                Range titleCells = sourceSheet.Cells[numericUpDown1.Value, 1];
                titleCells = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, titleCells.EntireRow);

                if (titleCells.Count <= 1)
                {
                    MessageBox.Show("表中至少有两列数据，否则没有意义。", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                var dirPath = fbd.SelectedPath;
                if (System.IO.Directory.Exists(fbd.SelectedPath) == false) return;

                if (dirPath.EndsWith("\\") == false)
                    dirPath += "\\";

                var existFiles = System.IO.Directory.GetFiles(dirPath);
                if (existFiles.Length > 0)
                {
                    var answer1 = MessageBox.Show("指定目录非空，继续操作可能会频繁询问【是否要覆盖同名文件】。建议另选一个空目录。\r\n\r\n确定要继续操作吗？", "Lunar Excel Addin", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (answer1 != DialogResult.Yes)
                        return;
                }

                var fieldName = cklstColumns.CheckedItems[0].ToString();
                var colIndex = -1;
                for (int i = 1; i <= titleCells.Count; i++)
                {
                    var tiCell = titleCells[1, i] as Range;
                    if (tiCell == null) continue;

                    var fieldValue = tiCell.Value.ToString();
                    if (fieldValue == fieldName)
                    {
                        colIndex = tiCell.Column;
                        break;
                    }
                }

                Range fieldCells = sourceSheet.Cells[(int)(numericUpDown1.Value + 1), colIndex];
                fieldCells = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, fieldCells.EntireColumn);

                var fileCount = 0;
                string preCellValue = null;
                int startRowIndex = (int)numericUpDown1.Value + 1;
                int endRowIndex = startRowIndex;

                Globals.ThisAddIn.Application.ScreenUpdating = false;

                var srcWorkBook = Globals.ThisAddIn.Application.ActiveWorkbook;
                Workbook newWorkBook = null;
                Worksheet newSheet = null;

                for (int i = (int)numericUpDown1.Value + 1; i <= numericUpDown1.Value + sourceSheet.UsedRange.Rows.Count; i++)
                {
                    Range rng = sourceSheet.Cells[i, colIndex] as Range;
                    if (rng.Value == null || string.IsNullOrWhiteSpace(rng.Value.ToString()))
                    {
                        continue;
                    }

                    if (preCellValue == null)
                    {
                        preCellValue = rng.Value.ToString();
                        continue;
                    }

                    if (preCellValue == rng.Value.ToString())
                    {
                        continue;
                    }

                    // 如果当前单元格的值和上一行同列单元格值不同，说明上一行是前一类值的最后一行。
                    endRowIndex = i - 1;
                    var usedrange = sourceSheet.UsedRange;

                    if (newWorkBook == null)
                    {
                        newWorkBook = Globals.ThisAddIn.Application.Workbooks.Add();
                    }

                    newSheet = newWorkBook.Sheets.Add();

                    titleCells.Copy(newSheet.Cells[1, 1]);

                    Range startCell = sourceSheet.Cells[startRowIndex, colIndex];
                    Range startRow = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, startCell.EntireRow);
                    Range endCell = sourceSheet.Cells[endRowIndex, colIndex];
                    Range endRow = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, endCell.EntireRow);

                    Range newTableData = sourceSheet.Range[startRow, endRow];
                    newTableData.Copy(newSheet.Cells[2, 1]);

                    // 只保留最后一个表
                    for (int si = newWorkBook.Sheets.Count; si >= 1; si--)
                    {
                        var sheet = newWorkBook.Sheets[si];
                        if (sheet == newSheet) continue;

                        Globals.ThisAddIn.Application.DisplayAlerts = false;
                        sheet.Delete();//删除sheet的方法
                        Globals.ThisAddIn.Application.DisplayAlerts = true;
                    }

                    newSheet.Name = sourceSheet.Name;  //必须放在最后，否则会出现同名错误。

                    var filePath = dirPath + preCellValue + ".xlsx";
                    newWorkBook.SaveAs(filePath);

                    fileCount++;

                    // 下一张表开始了。
                    preCellValue = rng.Value.ToString();
                    startRowIndex = i;
                }

                newSheet = newWorkBook.Sheets.Add();
                newSheet.Cells.Font.Name = "SimSun";
                newSheet.Cells.Font.Size = 11;

                Range startCell2 = sourceSheet.Cells[startRowIndex, colIndex];
                Range startRow2 = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, startCell2.EntireRow);

                Excel.Range lastCell = sourceSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
                Range endRow2 = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, lastCell.EntireRow);

                Range newTableData2 = sourceSheet.Range[startRow2, endRow2];

                titleCells.Copy(newSheet.Cells[1, 1]);
                newTableData2.Copy(newSheet.Cells[2, 1]);

                // 只保留最后一个表
                for (int si = newWorkBook.Sheets.Count; si >= 1; si--)
                {
                    var sheet = newWorkBook.Sheets[si];
                    if (sheet == newSheet) continue;

                    Globals.ThisAddIn.Application.DisplayAlerts = false;
                    sheet.Delete();//删除sheet的方法
                    Globals.ThisAddIn.Application.DisplayAlerts = true;
                }

                //newSheet.Name = sourceSheet.Name;  //必须放在最后，否则会出现同名错误。
                //newSheet.Cells.Font.Name = "SimSun";
                //newSheet.Cells.Font.Size = 11;

                var filePath2 = dirPath + preCellValue + ".xlsx";
                newWorkBook.SaveAs(filePath2, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                newWorkBook.Close();

                fileCount++;

                Globals.ThisAddIn.Application.ScreenUpdating = true;

                MessageBox.Show($"共导出【{fileCount}】个 Excel 文件到目录【{dirPath}】下。", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                Globals.ThisAddIn.Application.ScreenUpdating = true;
                MessageBox.Show(ex.Message, "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private string FormatFileName(string src)
        {
            var result = src.Replace("\\", "").Replace("/", "").Replace(":", "").Replace("*", "").Replace(">", "")
                .Replace("<", "").Replace("?", "").Replace("|", "").Replace("\"", "");

            if (string.IsNullOrWhiteSpace(result))
            {
                return "NewExcelFile-" + DateTime.Now.Date.ToShortDateString() + " " + DateTime.Now.TimeOfDay.ToString();
            }

            return result;
        }

        private Worksheet sourceSheet = null;

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            FillCellsToListBox();
        }

        private void btnSplitToNewSheets_Click(object sender, EventArgs e)
        {
            try
            {
                if (sourceSheet == null)
                {
                    MessageBox.Show("源数据表为 null。", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (cklstColumns.CheckedItems.Count != 1)
                {
                    MessageBox.Show("只允许选择一个字段来切分文件。", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (sourceSheet.UsedRange.Rows.Count < 2)
                {
                    MessageBox.Show("表中至少有两行数据，第一行应是标题行。", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var answer = MessageBox.Show("执行此操作之前，必须按基准字段先执行【排序】操作。\r\n\r\n" +
                    "如果出现不连续的同值字段值，很容易因误操作造成【误覆盖】或【生成的文件不完整】。\r\n\r\n" +
                    "你确定要继续吗？",
                    "Lunar Excel Addin", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (answer != DialogResult.Yes)
                {
                    return;
                }

                // 基本思路：
                //   1. 取标题栏
                //   2. 创建新表
                //   3. 在新表中填充同一个字段值的所有列
                //   4. 保存新表（用字段值）
                Range titleCells = sourceSheet.Cells[numericUpDown1.Value, 1];
                titleCells = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, titleCells.EntireRow);

                if (titleCells.Count <= 1)
                {
                    MessageBox.Show("表中至少有两列数据，否则没有意义。", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var fieldName = cklstColumns.CheckedItems[0].ToString();
                var colIndex = -1;
                for (int i = 1; i <= titleCells.Count; i++)
                {
                    var tiCell = titleCells[1, i] as Range;
                    if (tiCell == null) continue;

                    var fieldValue = tiCell.Value.ToString();
                    if (fieldValue == fieldName)
                    {
                        colIndex = tiCell.Column;
                        break;
                    }
                }

                Range fieldCells = sourceSheet.Cells[(int)(numericUpDown1.Value + 1), colIndex];
                fieldCells = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, fieldCells.EntireColumn);

                var sheetsCount = 0;
                string preCellValue = null;
                int startRowIndex = (int)numericUpDown1.Value + 1;
                int endRowIndex = startRowIndex;

                Globals.ThisAddIn.Application.ScreenUpdating = false;

                var srcWorkBook = Globals.ThisAddIn.Application.ActiveWorkbook;
                Workbook newWorkBook = null;
                Worksheet newSheet = null;

                for (int i = (int)numericUpDown1.Value + 1; i <= numericUpDown1.Value + sourceSheet.UsedRange.Rows.Count; i++)
                {
                    Range rng = sourceSheet.Cells[i, colIndex] as Range;
                    if (rng.Value == null || string.IsNullOrWhiteSpace(rng.Value.ToString()))
                    {
                        continue;
                    }

                    if (preCellValue == null)
                    {
                        preCellValue = rng.Value.ToString();
                        continue;
                    }

                    if (preCellValue == rng.Value.ToString())
                    {
                        continue;
                    }

                    // 如果当前单元格的值和上一行同列单元格值不同，说明上一行是前一类值的最后一行。
                    endRowIndex = i - 1;
                    var usedrange = sourceSheet.UsedRange;

                    if (newWorkBook == null)
                    {
                        newWorkBook = Globals.ThisAddIn.Application.Workbooks.Add();
                    }

                    if(sheetsCount <= 0)
                    {
                        newSheet = newWorkBook.Sheets["Sheet1"];
                    }
                    else
                    {
                        newSheet = newWorkBook.Sheets.Add(Type.Missing, Globals.ThisAddIn.Application.ActiveSheet, 1, XlSheetType.xlWorksheet);
                    }

                    titleCells.Copy(newSheet.Cells[1, 1]);

                    Range startCell = sourceSheet.Cells[startRowIndex, colIndex];
                    Range startRow = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, startCell.EntireRow);
                    Range endCell = sourceSheet.Cells[endRowIndex, colIndex];
                    Range endRow = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, endCell.EntireRow);

                    Range newTableData = sourceSheet.Range[startRow, endRow];
                    newTableData.Copy(newSheet.Cells[2, 1]);

                    newSheet.Name = preCellValue;

                    sheetsCount++;

                    // 下一张表开始了。
                    preCellValue = rng.Value.ToString();
                    startRowIndex = i;
                }

                newSheet = newWorkBook.Sheets.Add(Type.Missing, newWorkBook.ActiveSheet);
                newSheet.Cells.Font.Name = "SimSun";
                newSheet.Cells.Font.Size = 11;
                newSheet.Name = preCellValue;

                Range startCell2 = sourceSheet.Cells[startRowIndex, colIndex];
                Range startRow2 = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, startCell2.EntireRow);

                Excel.Range lastCell = sourceSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
                Range endRow2 = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, lastCell.EntireRow);

                Range newTableData2 = sourceSheet.Range[startRow2, endRow2];

                titleCells.Copy(newSheet.Cells[1, 1]);
                newTableData2.Copy(newSheet.Cells[2, 1]);

                sheetsCount++;

                Globals.ThisAddIn.Application.ScreenUpdating = true;
                this.Close();

                MessageBox.Show($"已切分成【{sheetsCount}】个表！", "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Globals.ThisAddIn.Application.ScreenUpdating = true;
                MessageBox.Show(ex.Message, "Lunar Excel Addin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cmbSheetNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            sourceSheet = Globals.ThisAddIn.Application.Sheets[cmbSheetNames.SelectedIndex + 1];
            var usedColumnCount = sourceSheet.UsedRange.Columns.Count;
            if (usedColumnCount <= 0 || sourceSheet.UsedRange.Count <= 0)
            {
                MessageBox.Show($"表 【{cmbSheetNames.Text}】 中没有可供查询的数据！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            numericUpDown1.Maximum = sourceSheet.UsedRange.Row + sourceSheet.UsedRange.Rows.Count - 1;
            numericUpDown1.Value = sourceSheet.UsedRange.Row;
            //FillCellsToListBox();//没必要
        }
    }
}
