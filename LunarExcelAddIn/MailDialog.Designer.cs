﻿namespace LunarExcelAddIn
{
    partial class MailDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gpDataSource = new System.Windows.Forms.GroupBox();
            this.cmbDataSourceSheet = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnFstRecord = new System.Windows.Forms.Button();
            this.btnPreviewRecord = new System.Windows.Forms.Button();
            this.btnNextRecord = new System.Windows.Forms.Button();
            this.btnLastRecord = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.gpTemplate = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbTemplateSheet = new System.Windows.Forms.ComboBox();
            this.btnPrintTemplate = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.gpSheetNameField = new System.Windows.Forms.GroupBox();
            this.cmbSheetNameField = new System.Windows.Forms.ComboBox();
            this.ckxSaveRecordToNewSheet = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnSaveRecordAsFile = new System.Windows.Forms.Button();
            this.gbColumns = new System.Windows.Forms.GroupBox();
            this.ckxAddPageBreak = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numColumns = new System.Windows.Forms.NumericUpDown();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.gpDataSource.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.gpTemplate.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.gpSheetNameField.SuspendLayout();
            this.gbColumns.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numColumns)).BeginInit();
            this.SuspendLayout();
            // 
            // gpDataSource
            // 
            this.gpDataSource.Controls.Add(this.cmbDataSourceSheet);
            this.gpDataSource.Location = new System.Drawing.Point(16, 135);
            this.gpDataSource.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpDataSource.Name = "gpDataSource";
            this.gpDataSource.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpDataSource.Size = new System.Drawing.Size(408, 63);
            this.gpDataSource.TabIndex = 0;
            this.gpDataSource.TabStop = false;
            this.gpDataSource.Text = "请指定源数据表：";
            // 
            // cmbDataSourceSheet
            // 
            this.cmbDataSourceSheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDataSourceSheet.FormattingEnabled = true;
            this.cmbDataSourceSheet.Location = new System.Drawing.Point(9, 27);
            this.cmbDataSourceSheet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbDataSourceSheet.Name = "cmbDataSourceSheet";
            this.cmbDataSourceSheet.Size = new System.Drawing.Size(391, 28);
            this.cmbDataSourceSheet.Sorted = true;
            this.cmbDataSourceSheet.TabIndex = 0;
            this.cmbDataSourceSheet.DropDown += new System.EventHandler(this.cmbDataSourceSheet_DropDown);
            this.cmbDataSourceSheet.SelectedIndexChanged += new System.EventHandler(this.cmbDataSourceSheet_SelectedIndexChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Controls.Add(this.btnFstRecord, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnPreviewRecord, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNextRecord, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnLastRecord, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown1, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(25, 215);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(306, 53);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // btnFstRecord
            // 
            this.btnFstRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFstRecord.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnFstRecord.Location = new System.Drawing.Point(3, 4);
            this.btnFstRecord.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFstRecord.Name = "btnFstRecord";
            this.btnFstRecord.Size = new System.Drawing.Size(39, 45);
            this.btnFstRecord.TabIndex = 0;
            this.btnFstRecord.Text = "|<";
            this.btnFstRecord.UseVisualStyleBackColor = true;
            this.btnFstRecord.Click += new System.EventHandler(this.btnFstRecord_Click);
            // 
            // btnPreviewRecord
            // 
            this.btnPreviewRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPreviewRecord.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPreviewRecord.Location = new System.Drawing.Point(48, 4);
            this.btnPreviewRecord.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPreviewRecord.Name = "btnPreviewRecord";
            this.btnPreviewRecord.Size = new System.Drawing.Size(39, 45);
            this.btnPreviewRecord.TabIndex = 1;
            this.btnPreviewRecord.Text = "<";
            this.btnPreviewRecord.UseVisualStyleBackColor = true;
            this.btnPreviewRecord.Click += new System.EventHandler(this.btnPreviewRecord_Click);
            // 
            // btnNextRecord
            // 
            this.btnNextRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextRecord.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNextRecord.Location = new System.Drawing.Point(215, 4);
            this.btnNextRecord.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNextRecord.Name = "btnNextRecord";
            this.btnNextRecord.Size = new System.Drawing.Size(39, 45);
            this.btnNextRecord.TabIndex = 3;
            this.btnNextRecord.Text = ">";
            this.btnNextRecord.UseVisualStyleBackColor = true;
            this.btnNextRecord.Click += new System.EventHandler(this.btnNextRecord_Click);
            // 
            // btnLastRecord
            // 
            this.btnLastRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLastRecord.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLastRecord.Location = new System.Drawing.Point(260, 4);
            this.btnLastRecord.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLastRecord.Name = "btnLastRecord";
            this.btnLastRecord.Size = new System.Drawing.Size(43, 45);
            this.btnLastRecord.TabIndex = 4;
            this.btnLastRecord.Text = ">|";
            this.btnLastRecord.UseVisualStyleBackColor = true;
            this.btnLastRecord.Click += new System.EventHandler(this.btnLastRecord_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numericUpDown1.Location = new System.Drawing.Point(102, 13);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(98, 26);
            this.numericUpDown1.TabIndex = 2;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numericUpDown1_KeyDown);
            // 
            // gpTemplate
            // 
            this.gpTemplate.Controls.Add(this.label2);
            this.gpTemplate.Controls.Add(this.cmbTemplateSheet);
            this.gpTemplate.Location = new System.Drawing.Point(16, 13);
            this.gpTemplate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpTemplate.Name = "gpTemplate";
            this.gpTemplate.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpTemplate.Size = new System.Drawing.Size(408, 108);
            this.gpTemplate.TabIndex = 1;
            this.gpTemplate.TabStop = false;
            this.gpTemplate.Text = "请指定数据模板表：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(8, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(314, 40);
            this.label2.TabIndex = 1;
            this.label2.Text = "★注意：尽量不要在模板中使用合并单元格！！！\r\n　　　　因为行高、列宽不会太准。";
            // 
            // cmbTemplateSheet
            // 
            this.cmbTemplateSheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTemplateSheet.FormattingEnabled = true;
            this.cmbTemplateSheet.Location = new System.Drawing.Point(9, 26);
            this.cmbTemplateSheet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbTemplateSheet.Name = "cmbTemplateSheet";
            this.cmbTemplateSheet.Size = new System.Drawing.Size(391, 28);
            this.cmbTemplateSheet.Sorted = true;
            this.cmbTemplateSheet.TabIndex = 0;
            this.cmbTemplateSheet.DropDown += new System.EventHandler(this.cmbTemplateSheet_DropDown);
            this.cmbTemplateSheet.SelectedIndexChanged += new System.EventHandler(this.cmbTemplateSheet_SelectedIndexChanged);
            // 
            // btnPrintTemplate
            // 
            this.btnPrintTemplate.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrintTemplate.Location = new System.Drawing.Point(338, 225);
            this.btnPrintTemplate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPrintTemplate.Name = "btnPrintTemplate";
            this.btnPrintTemplate.Size = new System.Drawing.Size(78, 32);
            this.btnPrintTemplate.TabIndex = 2;
            this.btnPrintTemplate.Text = "打印(&P)";
            this.btnPrintTemplate.UseVisualStyleBackColor = true;
            this.btnPrintTemplate.Click += new System.EventHandler(this.btnPrintTemplate_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 662);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 18, 0);
            this.statusStrip1.Size = new System.Drawing.Size(437, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // gpSheetNameField
            // 
            this.gpSheetNameField.Controls.Add(this.cmbSheetNameField);
            this.gpSheetNameField.Enabled = false;
            this.gpSheetNameField.Location = new System.Drawing.Point(17, 276);
            this.gpSheetNameField.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpSheetNameField.Name = "gpSheetNameField";
            this.gpSheetNameField.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gpSheetNameField.Size = new System.Drawing.Size(408, 79);
            this.gpSheetNameField.TabIndex = 4;
            this.gpSheetNameField.TabStop = false;
            this.gpSheetNameField.Text = "请选择用作表名的字段：";
            // 
            // cmbSheetNameField
            // 
            this.cmbSheetNameField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSheetNameField.Enabled = false;
            this.cmbSheetNameField.FormattingEnabled = true;
            this.cmbSheetNameField.Location = new System.Drawing.Point(9, 33);
            this.cmbSheetNameField.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbSheetNameField.Name = "cmbSheetNameField";
            this.cmbSheetNameField.Size = new System.Drawing.Size(390, 28);
            this.cmbSheetNameField.TabIndex = 0;
            // 
            // ckxSaveRecordToNewSheet
            // 
            this.ckxSaveRecordToNewSheet.AutoSize = true;
            this.ckxSaveRecordToNewSheet.Location = new System.Drawing.Point(25, 363);
            this.ckxSaveRecordToNewSheet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ckxSaveRecordToNewSheet.Name = "ckxSaveRecordToNewSheet";
            this.ckxSaveRecordToNewSheet.Size = new System.Drawing.Size(126, 24);
            this.ckxSaveRecordToNewSheet.TabIndex = 1;
            this.ckxSaveRecordToNewSheet.Text = "每记录单独成表";
            this.ckxSaveRecordToNewSheet.UseVisualStyleBackColor = true;
            this.ckxSaveRecordToNewSheet.CheckedChanged += new System.EventHandler(this.ckxSaveRecordToNewSheet_CheckedChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipTitle = "将当前记录填充到模板表中并保存为 Excel 文件。";
            // 
            // btnSaveRecordAsFile
            // 
            this.btnSaveRecordAsFile.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSaveRecordAsFile.Location = new System.Drawing.Point(334, 424);
            this.btnSaveRecordAsFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveRecordAsFile.Name = "btnSaveRecordAsFile";
            this.btnSaveRecordAsFile.Size = new System.Drawing.Size(82, 32);
            this.btnSaveRecordAsFile.TabIndex = 8;
            this.btnSaveRecordAsFile.Text = "保存(&S)";
            this.toolTip1.SetToolTip(this.btnSaveRecordAsFile, "将当前记录填充到模板中并保存为一个单独的文件。");
            this.btnSaveRecordAsFile.UseVisualStyleBackColor = true;
            this.btnSaveRecordAsFile.Click += new System.EventHandler(this.btnSaveRecordAsFile_Click);
            // 
            // gbColumns
            // 
            this.gbColumns.Controls.Add(this.ckxAddPageBreak);
            this.gbColumns.Controls.Add(this.label1);
            this.gbColumns.Controls.Add(this.numColumns);
            this.gbColumns.Location = new System.Drawing.Point(17, 395);
            this.gbColumns.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbColumns.Name = "gbColumns";
            this.gbColumns.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbColumns.Size = new System.Drawing.Size(314, 154);
            this.gbColumns.TabIndex = 6;
            this.gbColumns.TabStop = false;
            this.gbColumns.Text = "栏数：";
            // 
            // ckxAddPageBreak
            // 
            this.ckxAddPageBreak.AutoSize = true;
            this.ckxAddPageBreak.Checked = true;
            this.ckxAddPageBreak.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckxAddPageBreak.Location = new System.Drawing.Point(11, 64);
            this.ckxAddPageBreak.Name = "ckxAddPageBreak";
            this.ckxAddPageBreak.Size = new System.Drawing.Size(175, 24);
            this.ckxAddPageBreak.TabIndex = 7;
            this.ckxAddPageBreak.Text = "自动添加水平分页符(&H)";
            this.ckxAddPageBreak.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 40);
            this.label1.TabIndex = 1;
            this.label1.Text = "如果需要严格复制模板行列尺寸，\r\n请务必使用1栏。";
            // 
            // numColumns
            // 
            this.numColumns.Location = new System.Drawing.Point(9, 31);
            this.numColumns.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.numColumns.Maximum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.numColumns.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numColumns.Name = "numColumns";
            this.numColumns.Size = new System.Drawing.Size(298, 26);
            this.numColumns.TabIndex = 0;
            this.numColumns.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numColumns.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numColumns.ValueChanged += new System.EventHandler(this.numColumns_ValueChanged);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox1.Location = new System.Drawing.Point(0, 556);
            this.textBox1.Margin = new System.Windows.Forms.Padding(11, 14, 11, 14);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(437, 106);
            this.textBox1.TabIndex = 7;
            this.textBox1.Text = "　　以数据源表第一个有效行为字段名！在模板表上以这些字段名命名单元格。\r\n　　注意：\r\n　　① 单元格命名不要用数字开头，否则要注意自动添加的 _ 字符；\r\n　　" +
    "② 工作簿中如果有多个模板时，要注意单元格命名不能重复。";
            // 
            // MailDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 684);
            this.Controls.Add(this.btnSaveRecordAsFile);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.gbColumns);
            this.Controls.Add(this.gpSheetNameField);
            this.Controls.Add(this.ckxSaveRecordToNewSheet);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnPrintTemplate);
            this.Controls.Add(this.gpTemplate);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.gpDataSource);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "MailDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "邮件合并";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MailDialog_FormClosing);
            this.gpDataSource.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.gpTemplate.ResumeLayout(false);
            this.gpTemplate.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.gpSheetNameField.ResumeLayout(false);
            this.gbColumns.ResumeLayout(false);
            this.gbColumns.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numColumns)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gpDataSource;
        private System.Windows.Forms.ComboBox cmbDataSourceSheet;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnFstRecord;
        private System.Windows.Forms.Button btnPreviewRecord;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button btnNextRecord;
        private System.Windows.Forms.Button btnLastRecord;
        private System.Windows.Forms.GroupBox gpTemplate;
        private System.Windows.Forms.ComboBox cmbTemplateSheet;
        private System.Windows.Forms.Button btnPrintTemplate;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox gpSheetNameField;
        private System.Windows.Forms.CheckBox ckxSaveRecordToNewSheet;
        private System.Windows.Forms.ComboBox cmbSheetNameField;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox gbColumns;
        private System.Windows.Forms.NumericUpDown numColumns;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox ckxAddPageBreak;
        private System.Windows.Forms.Button btnSaveRecordAsFile;
        private System.Windows.Forms.Label label2;
    }
}