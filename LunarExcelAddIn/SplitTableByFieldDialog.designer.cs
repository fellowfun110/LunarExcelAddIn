﻿namespace LunarExcelAddIn
{
    partial class SplitTableByFieldDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaveRecordsAsFile = new System.Windows.Forms.Button();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.cmbSheetNames = new System.Windows.Forms.ComboBox();
            this.gpDataSource = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.cklstColumns = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSplitToNewSheets = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.gpDataSource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSaveRecordsAsFile
            // 
            this.btnSaveRecordsAsFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveRecordsAsFile.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSaveRecordsAsFile.Location = new System.Drawing.Point(286, 448);
            this.btnSaveRecordsAsFile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSaveRecordsAsFile.Name = "btnSaveRecordsAsFile";
            this.btnSaveRecordsAsFile.Size = new System.Drawing.Size(200, 32);
            this.btnSaveRecordsAsFile.TabIndex = 14;
            this.btnSaveRecordsAsFile.Text = "切分成一系列文件(&D)";
            this.btnSaveRecordsAsFile.UseVisualStyleBackColor = true;
            this.btnSaveRecordsAsFile.Click += new System.EventHandler(this.btnSaveRecordsAsFile_Click);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(296, 17);
            this.toolStripStatusLabel1.Text = "请手动设置列标题所在的行号，并指定要使用的字段。";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 522);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 21, 0);
            this.statusStrip1.Size = new System.Drawing.Size(511, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // cmbSheetNames
            // 
            this.cmbSheetNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSheetNames.FormattingEnabled = true;
            this.cmbSheetNames.Location = new System.Drawing.Point(11, 32);
            this.cmbSheetNames.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbSheetNames.Name = "cmbSheetNames";
            this.cmbSheetNames.Size = new System.Drawing.Size(456, 28);
            this.cmbSheetNames.Sorted = true;
            this.cmbSheetNames.TabIndex = 0;
            this.cmbSheetNames.SelectedIndexChanged += new System.EventHandler(this.cmbSheetNames_SelectedIndexChanged);
            // 
            // gpDataSource
            // 
            this.gpDataSource.Controls.Add(this.cmbSheetNames);
            this.gpDataSource.Location = new System.Drawing.Point(19, 17);
            this.gpDataSource.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gpDataSource.Name = "gpDataSource";
            this.gpDataSource.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gpDataSource.Size = new System.Drawing.Size(476, 76);
            this.gpDataSource.TabIndex = 7;
            this.gpDataSource.TabStop = false;
            this.gpDataSource.Text = "请指定源数据表：";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 494);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(401, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "按指定的字段分类拆分成不同的表格文件，输出到指定目录下。";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown1.Location = new System.Drawing.Point(29, 140);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(457, 26);
            this.numericUpDown1.TabIndex = 17;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 106);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(191, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "请输入列标题所在行的行号：";
            // 
            // cklstColumns
            // 
            this.cklstColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cklstColumns.FormattingEnabled = true;
            this.cklstColumns.Location = new System.Drawing.Point(29, 217);
            this.cklstColumns.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cklstColumns.Name = "cklstColumns";
            this.cklstColumns.Size = new System.Drawing.Size(456, 214);
            this.cklstColumns.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 181);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(205, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "请选择需要显示在结果中的列：";
            // 
            // btnSplitToNewSheets
            // 
            this.btnSplitToNewSheets.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSplitToNewSheets.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSplitToNewSheets.Location = new System.Drawing.Point(28, 448);
            this.btnSplitToNewSheets.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSplitToNewSheets.Name = "btnSplitToNewSheets";
            this.btnSplitToNewSheets.Size = new System.Drawing.Size(225, 32);
            this.btnSplitToNewSheets.TabIndex = 20;
            this.btnSplitToNewSheets.Text = "切分到新文件中的各工作表(&S)";
            this.btnSplitToNewSheets.UseVisualStyleBackColor = true;
            this.btnSplitToNewSheets.Click += new System.EventHandler(this.btnSplitToNewSheets_Click);
            // 
            // SplitTableByFieldDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 544);
            this.Controls.Add(this.btnSplitToNewSheets);
            this.Controls.Add(this.cklstColumns);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSaveRecordsAsFile);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.gpDataSource);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "SplitTableByFieldDialog";
            this.Text = "按字段拆分表格文件";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.gpDataSource.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSaveRecordsAsFile;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ComboBox cmbSheetNames;
        private System.Windows.Forms.GroupBox gpDataSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox cklstColumns;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSplitToNewSheets;
    }
}