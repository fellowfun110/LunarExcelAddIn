﻿using System.Collections.Generic;

namespace LunarExcelAddIn
{
    internal class RecordComparer : IComparer<Record>
    {
        private int i;

        public RecordComparer(int i)
        {
            this.i = i;
        }

        public int Compare(Record x, Record y)
        {
            return x.Fields[i].Value.CompareTo(y.Fields[i].Value);
        }
    }
}