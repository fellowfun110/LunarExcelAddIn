﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;
using SHomeWorkshop.LunarSF.StudentInfosManager.ValidationRules;
using Microsoft.Office.Core;

namespace LunarExcelAddIn
{
    public partial class LunarRibbon
    {
        private void LunarRibbon_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void btnSelectEmptyCells_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.Selection.SpecialCells(
                Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeBlanks).Select();
        }

        private void btnAddComments_Click(object sender, RibbonControlEventArgs e)
        {
            var newComment = Globals.ThisAddIn.Application.InputBox("请输入批注文本：", "Lunar Excel AddIn", "示例文本") as string;
            if (newComment == null || newComment.Length <= 0) return;
            Range seledRng = Globals.ThisAddIn.Application.Selection;

            var date = DateTime.Now.ToLongDateString();
            var time = DateTime.Now.ToLongTimeString();

            Globals.ThisAddIn.Application.ScreenUpdating = false;

            foreach (Range r in seledRng)
            {
                r.ClearComments();//如果本来就有注释。

                r.AddComment($"{date} {time}\r\n\r\n{newComment}");
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        private void btnIdCardNumberValidate_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            Range usedRng = sheet.UsedRange;
            Range rng = Globals.ThisAddIn.Application.Intersect(usedRng, seledRng);
            if (rng == null || rng.Count <= 0)
            {
                MessageBox.Show("没有在已使用的区域选定任何单元格。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Globals.ThisAddIn.Application.ScreenUpdating = false;
            Range errRanges = null;
            Range rightRanges = null;
            foreach (Range r in rng)
            {
                var text = r.Value as string;
                if (ValidateIdCardNumber(text) == false)
                {
                    if (errRanges == null)
                    {
                        errRanges = r;
                    }
                    else errRanges = Globals.ThisAddIn.Application.Union(errRanges, r);
                }
                else
                {
                    if (rightRanges == null)
                    {
                        rightRanges = r;
                    }
                    else rightRanges = Globals.ThisAddIn.Application.Union(rightRanges, r);
                }
            }

            int errCount = 0;
            int rightCount = 0;
            int ignoredCount = seledRng.Count - rng.Count;

            if (rightRanges != null && rightRanges.Count > 0)
            {
                rightRanges.Interior.ColorIndex = 0;
                rightCount += rightRanges.Count;
            }

            if (errRanges != null && errRanges.Count > 0)
            {
                errRanges.Interior.ColorIndex = 6;
                errCount += errRanges.Count;
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;

            MessageBox.Show($"有 {rightCount} 个单元格〖通过〗校验！\r\n" +
                $"有 {errCount} 个单元格【未通过】校验！\r\n\r\n" +
                ((ignoredCount > 0) ? $"有 {ignoredCount} 个单元格虽然被选中，但既无内容，又无格式，故未进行校验！" : "")
                , "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private bool ValidateIdCardNumber(string text)
        {
            if (string.IsNullOrWhiteSpace(text)) return false;
            return (_IDCard.CheckCidInfo18(text) == "" || _IDCard.CheckCidInfo15(text) == "");
        }

        /// <summary>
        /// 校验银行卡号（仅19位）。
        /// </summary>
        private void btnBankCardNumberValidate_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            Range usedRng = sheet.UsedRange;
            Range rng = Globals.ThisAddIn.Application.Intersect(usedRng, seledRng);
            if (rng == null || rng.Count <= 0)
            {
                MessageBox.Show("没有在已使用的区域选定任何单元格。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Globals.ThisAddIn.Application.ScreenUpdating = false;
            Range errRanges = null;
            Range rightRanges = null;
            foreach (Range r in rng)
            {
                var text = r.Value as string;
                if (SavingCardNumberRule.ValidateSavingCardNumber(text) == false)
                {
                    if (errRanges == null)
                    {
                        errRanges = r;
                    }
                    else errRanges = Globals.ThisAddIn.Application.Union(errRanges, r);
                }
                else
                {
                    if (rightRanges == null)
                    {
                        rightRanges = r;
                    }
                    else rightRanges = Globals.ThisAddIn.Application.Union(rightRanges, r);
                }
            }

            int errCount = 0;
            int rightCount = 0;
            int ignoredCount = seledRng.Count - rng.Count;

            if (rightRanges != null && rightRanges.Count > 0)
            {
                rightRanges.Interior.ColorIndex = 0;
                rightCount += rightRanges.Count;
            }

            if (errRanges != null && errRanges.Count > 0)
            {
                errRanges.Interior.ColorIndex = 6;
                errCount += errRanges.Count;
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;

            MessageBox.Show($"有 {rightCount} 个单元格〖通过〗校验！\r\n" +
                $"有 {errCount} 个单元格【未通过】校验！\r\n\r\n" +
                ((ignoredCount > 0) ? $"有 {ignoredCount} 个单元格虽然被选中，但既无内容，又无格式，故未进行校验！" : "")
                , "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// 设置偶数行的背景色为浅灰色，奇数行为背景色为透明色。
        /// </summary>
        private void btnSetBackgroundBetweenLines_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;

            Range rnges = seledRng.Resize[seledRng.Rows.Count, 1];
            int startRowIndex = rnges.Cells[1, 1].Row;

            Globals.ThisAddIn.Application.ScreenUpdating = false;

            foreach (Range rng in rnges)
            {
                int rowIndex = rng.Row;
                Range area = Globals.ThisAddIn.Application.Intersect(seledRng, rng.EntireRow);
                if ((rowIndex - startRowIndex) % 2 == 1)
                {
                    area.Interior.ColorIndex = 15;//15，浅灰色。//37，浅蓝色
                }
                else area.Interior.ColorIndex = 0;      //无色
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        /// <summary>
        /// 设置偶数列背景色为浅灰色，奇数列为透明色。
        /// </summary>
        private void btnSetBackgroundBetweenColumns_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;

            Range rnges = seledRng.Resize[1, seledRng.Columns.Count];
            int startColumnIndex = rnges.Cells[1, 1].Column;

            Globals.ThisAddIn.Application.ScreenUpdating = false;

            foreach (Range rng in rnges)
            {
                int columnIndex = rng.Column;
                Range area = Globals.ThisAddIn.Application.Intersect(seledRng, rng.EntireColumn);
                if ((columnIndex - startColumnIndex) % 2 == 1)
                {
                    area.Interior.ColorIndex = 15;//15，浅灰色。//37，浅蓝色
                }
                else area.Interior.ColorIndex = 0;      //无色
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        /// <summary>
        /// 设置所有框线。
        /// </summary>
        private void btnAddAllBorders_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            if (seledRng.Count <= 0) return;

            seledRng.Borders[XlBordersIndex.xlDiagonalDown].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlDiagonalUp].LineStyle = XlConstants.xlNone;

            Border borderLeft = seledRng.Borders[XlBordersIndex.xlEdgeLeft];
            borderLeft.LineStyle = XlLineStyle.xlContinuous;
            borderLeft.ColorIndex = 0;
            borderLeft.TintAndShade = 0;
            borderLeft.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            Border borderRight = seledRng.Borders[XlBordersIndex.xlEdgeRight];
            borderRight.LineStyle = XlLineStyle.xlContinuous;
            borderRight.ColorIndex = 0;
            borderRight.TintAndShade = 0;
            borderRight.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            Border borderTop = seledRng.Borders[XlBordersIndex.xlEdgeTop];
            borderTop.LineStyle = XlLineStyle.xlContinuous;
            borderTop.ColorIndex = 0;
            borderTop.TintAndShade = 0;
            borderTop.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            Border borderBottom = seledRng.Borders[XlBordersIndex.xlEdgeBottom];
            borderBottom.LineStyle = XlLineStyle.xlContinuous;
            borderBottom.ColorIndex = 0;
            borderBottom.TintAndShade = 0;
            borderBottom.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            Border borderInsideVertical = seledRng.Borders[XlBordersIndex.xlInsideVertical];
            borderInsideVertical.LineStyle = XlLineStyle.xlContinuous;
            borderInsideVertical.ColorIndex = 0;
            borderInsideVertical.TintAndShade = 0;
            borderInsideVertical.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            Border borderInsideHorizontal = seledRng.Borders[XlBordersIndex.xlInsideHorizontal];
            borderInsideHorizontal.LineStyle = XlLineStyle.xlContinuous;
            borderInsideHorizontal.ColorIndex = 0;
            borderInsideHorizontal.TintAndShade = 0;
            borderInsideHorizontal.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

        }

        /// <summary>
        /// 取消所有框线。
        /// </summary>
        private void btnClearAllBorders_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            if (seledRng.Count <= 0) return;

            seledRng.Borders[XlBordersIndex.xlDiagonalDown].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlDiagonalUp].LineStyle = XlConstants.xlNone;

            seledRng.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlInsideVertical].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlConstants.xlNone;
        }

        /// <summary>
        /// 只设置两侧有框线，用于需要从视觉上区分两块横向区域的场景。
        /// </summary>
        private void btnSetLeftAndRightBorder_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            if (seledRng.Count <= 0) return;

            seledRng.Borders[XlBordersIndex.xlDiagonalDown].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlDiagonalUp].LineStyle = XlConstants.xlNone;

            Border borderLeft = seledRng.Borders[XlBordersIndex.xlEdgeLeft];
            borderLeft.LineStyle = XlLineStyle.xlContinuous;
            borderLeft.ColorIndex = 0;
            borderLeft.TintAndShade = 0;
            borderLeft.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            Border borderRight = seledRng.Borders[XlBordersIndex.xlEdgeRight];
            borderRight.LineStyle = XlLineStyle.xlContinuous;
            borderRight.ColorIndex = 0;
            borderRight.TintAndShade = 0;
            borderRight.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            seledRng.Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlInsideVertical].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlConstants.xlNone;
        }

        private void btnOnlyTopAndBottomBorder_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            if (seledRng.Count <= 0) return;

            seledRng.Borders[XlBordersIndex.xlDiagonalDown].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlDiagonalUp].LineStyle = XlConstants.xlNone;

            Border borderTop = seledRng.Borders[XlBordersIndex.xlEdgeTop];
            borderTop.LineStyle = XlLineStyle.xlContinuous;
            borderTop.ColorIndex = 0;
            borderTop.TintAndShade = 0;
            borderTop.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            Border borderBottom = seledRng.Borders[XlBordersIndex.xlEdgeBottom];
            borderBottom.LineStyle = XlLineStyle.xlContinuous;
            borderBottom.ColorIndex = 0;
            borderBottom.TintAndShade = 0;
            borderBottom.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            seledRng.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlInsideVertical].LineStyle = XlConstants.xlNone;
            seledRng.Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlConstants.xlNone;
        }

        private void btnFind_Click(object sender, RibbonControlEventArgs e)
        {
            FindForm ff = new FindForm() { StartPosition = FormStartPosition.CenterScreen, };
            //ff.ShowDialog();//不能用Show()，因为数据可能出错。
            ff.Show();//为啥还是用Show()？这是因为用ShowDialog()实在不方便。
        }

        public static Microsoft.Office.Interop.Excel.Application App { get { return Globals.ThisAddIn.Application; } }

        public static Worksheet ActiveSheet { get { return Globals.ThisAddIn.Application.ActiveSheet; } }

        private void btnSetAreaBackColor_Click(object sender, RibbonControlEventArgs e)
        {
            if (ActiveSheet == null) return;
            Range seledRng = App.Selection;
            if (seledRng.Count <= 0) return;

            Range usedRange = ActiveSheet.UsedRange;
            Range rngs = App.Intersect(usedRange, seledRng);
            if (rngs.Count <= 0 || rngs.Columns.Count != 1)
            {
                MessageBox.Show("请选中一列数据的一部分。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Range previewRng = null;
            int previewColorIndex = 0;

            App.ScreenUpdating = false;
            foreach (Range rng in rngs)
            {
                if (previewRng == null)
                {
                    previewRng = rng;
                }

                Range lineArea = App.Intersect(rng.EntireRow, usedRange);

                //try
                //{
                if (rng.Value.ToString() != previewRng.Value.ToString())
                {
                    if (previewColorIndex == 0) previewColorIndex = 36;  //15,灰色;
                    else previewColorIndex = 0;
                }
                //}
                //catch   // 这里可能出现不同数据类型不能比较的异常。                {
                //{ 
                //    if (previewColorIndex == 0) previewColorIndex = 36;  //15,灰色;
                //    else previewColorIndex = 0;
                //}

                lineArea.Interior.ColorIndex = previewColorIndex;

                previewRng = rng;
            }
            App.ScreenUpdating = true;
        }

        private void btnCreateDirectoies_Click(object sender, RibbonControlEventArgs e)
        {
            if (ActiveSheet == null) return;
            Range seledRng = App.Selection;
            if (seledRng.Count <= 0) return;

            string parentPath = Globals.ThisAddIn.Application.InputBox("请输入要批量创建子目录的亲目录路径：", "Lunar Excel AddIn");

            StringBuilder errorInfo = new StringBuilder();

            Range usedRange = ActiveSheet.UsedRange;
            Range rngs = App.Intersect(usedRange, seledRng);
            if (rngs.Count <= 0 || rngs.Columns.Count != 1)
            {
                MessageBox.Show("请选中一些单元格，这些单元格中应该包括要创建的子目录的短名。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (parentPath.EndsWith("\\") == false) parentPath += "\\";

            try
            {
                if (System.IO.Directory.Exists(parentPath) == false)
                {
                    System.IO.Directory.CreateDirectory(parentPath);
                }

                Range selection = Globals.ThisAddIn.Application.Selection;

                foreach (Range cell in rngs)
                {
                    var newChildDirectoryPath = parentPath + cell.Value;
                    System.IO.Directory.CreateDirectory(newChildDirectoryPath);
                }
            }
            catch (Exception ex)
            {
                errorInfo.Append(ex.Message);
                errorInfo.Append("\r\n");
            }

            if (errorInfo.Length > 0)
            {
                MessageBox.Show(errorInfo.ToString(), "Lunar Excel AddIn");
            }
        }

        private void btn阅卷_Click(object sender, RibbonControlEventArgs e)
        {
            //基本思路：新建一张sheet存放
            //格式：年级、班级、姓名、作答、批改结果、得分
            if (ActiveSheet == null) return;
            var aSheet = ActiveSheet;
            Range seledRng = App.Selection;
            if (seledRng.Count <= 0) return;

            Range usedRange = aSheet.UsedRange;
            Range rngs = App.Intersect(usedRange, seledRng);

            if (rngs == null) return;

            if (rngs.Count <= 0 || rngs.Columns.Count <= 4 || rngs.Rows.Count <= 1)
            {
                MessageBox.Show("请选中所有答案数据。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int maxRow = usedRange.Rows.Count;
            int answerRowIndex = 0;
            for (int i = 1; i <= maxRow; i++)
            {
                var rng = aSheet.Cells[i, 3] as Microsoft.Office.Interop.Excel.Range;
                var txt = (rng.Value as string)?.Trim();
                if (txt == "参考答案" || txt == "答案")
                {
                    answerRowIndex = i;
                    break;
                }
            }

            if (answerRowIndex <= 0)
            {
                MessageBox.Show("没找到【参考答案】行。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            App.ScreenUpdating = false;

            var answerText = "####";
            var maxColumn = usedRange.Columns.Count;
            for (int i = 5; i <= maxColumn; i++)
            {
                var rng = aSheet.Cells[answerRowIndex, i] as Microsoft.Office.Interop.Excel.Range;
                var txt = (rng.Value as string)?.Trim();
                if (string.IsNullOrWhiteSpace(txt) == false)
                {
                    var letter = txt.Substring(0, 1);
                    if ("abcdABCDａｂｃｄＡＢＣＤ".Contains(letter))
                    {
                        answerText += letter;
                    }
                    else answerText += "?";   //问号表示异常答案。
                }
                else
                {
                    answerText += "!";        //惊叹号表示未提供答案。
                }
            }

            var newSheet = (Worksheet)Globals.ThisAddIn.Application.Sheets.Add(aSheet);
            newSheet.Cells.NumberFormatLocal = "@";
            newSheet.Cells[1, 1].Select();

            var trimChars = new char[] { '|', '?', '!' };

            //双层循环，写单元格内容。

            //newSheet.Range[newSheet.Cells[1, 1], newSheet.Cells[1, maxColumn]].Value = aSheet.Range[aSheet.Cells[1, 1], aSheet.Cells[1, maxColumn]].Value;
            (newSheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range).Value = "年级";
            (newSheet.Cells[1, 2] as Microsoft.Office.Interop.Excel.Range).Value = "班级";
            (newSheet.Cells[1, 3] as Microsoft.Office.Interop.Excel.Range).Value = "姓名";
            (newSheet.Cells[1, 4] as Microsoft.Office.Interop.Excel.Range).Value = "练习日期";

            (newSheet.Cells[1, 5] as Microsoft.Office.Interop.Excel.Range).Value = "学生答案";
            (newSheet.Cells[1, 6] as Microsoft.Office.Interop.Excel.Range).Value = "批改情况（+正确；-错误）";
            (newSheet.Cells[1, 7] as Microsoft.Office.Interop.Excel.Range).Value = "得分";

            var num = 1;
            for (int i = 2; i <= maxRow; i++)
            {
                if (i == answerRowIndex) continue;

                num++;

                var userAnswerText = "";
                var userResultText = "";
                var c = 0;
                var rightCount = 0;
                var errCount = 0;
                for (int j = 5; j <= Math.Min(answerText.Length, maxColumn); j++)
                {
                    var aTxt = answerText[j - 1];
                    var userAnswerRng = aSheet.Cells[i, j] as Microsoft.Office.Interop.Excel.Range;
                    var userAnswer = userAnswerRng.Value as string;
                    if (string.IsNullOrWhiteSpace(userAnswer))
                    {
                        userResultText += "?";      //表示未能正确批改。
                    }
                    else
                    {
                        userAnswer = userAnswer.Trim();

                        var letter = userAnswer.Substring(0, 1);
                        if ("abcdABCDａｂｃｄＡＢＣＤ".Contains(letter))
                        {
                            userAnswerText += letter;
                            if (letter[0] == aTxt)
                            {
                                userResultText += "+";
                                rightCount++;
                            }
                            else
                            {
                                userResultText += "-";
                                errCount++;
                            }

                            c++;

                            if (c == 5)
                            {
                                userAnswerText += "|";
                                userResultText += "|";
                                c = 0;
                            }
                        }
                    }
                }
                (newSheet.Cells[num, 1] as Microsoft.Office.Interop.Excel.Range).Value = aSheet.Cells[i, 1].Value;
                (newSheet.Cells[num, 2] as Microsoft.Office.Interop.Excel.Range).Value = aSheet.Cells[i, 2].Value;
                (newSheet.Cells[num, 3] as Microsoft.Office.Interop.Excel.Range).Value = aSheet.Cells[i, 3].Value;
                (newSheet.Cells[num, 4] as Microsoft.Office.Interop.Excel.Range).Value = aSheet.Cells[i, 4].Value;

                (newSheet.Cells[num, 5] as Microsoft.Office.Interop.Excel.Range).Value = userAnswerText.TrimEnd(trimChars);
                (newSheet.Cells[num, 6] as Microsoft.Office.Interop.Excel.Range).Value = userResultText.TrimEnd(trimChars);
                (newSheet.Cells[num, 7] as Microsoft.Office.Interop.Excel.Range).Value = $"做对：{rightCount}题，做了：{rightCount + errCount}题";
            }
            App.ScreenUpdating = true;
        }

        private MailDialog mailDialog = new MailDialog();

        private void btnMailCop_Click(object sender, RibbonControlEventArgs e)
        {
            mailDialog.Show();
        }

        private void btnFindRepeatRecords_Click(object sender, RibbonControlEventArgs e)
        {
            var frForm = new FindRepeatRecords();
            frForm.Show();
        }

        private void btnSplitByField_Click(object sender, RibbonControlEventArgs e)
        {
            var spliterDialog = new SplitTableByFieldDialog();
            spliterDialog.ShowDialog();
        }

        private RegexInputBox regInputBox = new RegexInputBox();

        private void btnValidateWithRegex_Click(object sender, RibbonControlEventArgs e)
        {
            regInputBox.Show();
        }

        private void btnAggregateByFeild_Click(object sender, RibbonControlEventArgs e)
        {
            var aggregateDialog = new AggregateDialog(false);
            aggregateDialog.Show();
        }

        private void btnAutoAggregateByFeild_Click(object sender, RibbonControlEventArgs e)
        {
            var aggregateDialog = new AggregateDialog(true);
            aggregateDialog.Show();
        }

        private void btnSetBackgroundByFirstChar_Click(object sender, RibbonControlEventArgs e)
        {
            if (ActiveSheet == null) return;
            Range seledRng = App.Selection;
            if (seledRng.Count <= 0) return;

            Range usedRange = ActiveSheet.UsedRange;
            Range rngs = App.Intersect(usedRange, seledRng);
            if (rngs.Count <= 0 || rngs.Columns.Count != 1)
            {
                MessageBox.Show("请选中一列数据的一部分。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Range previewRng = null;
            int previewColorIndex = 0;

            App.ScreenUpdating = false;
            foreach (Range rng in rngs)
            {
                if (previewRng == null)
                {
                    previewRng = rng;
                }

                Range lineArea = App.Intersect(rng.EntireRow, usedRange);
                var rngVal = rng.Value.ToString();
                var preRngVal = previewRng.Value.ToString();
                if (string.IsNullOrEmpty(rngVal) == false && string.IsNullOrEmpty(preRngVal) == false)
                {
                    if (rngVal[0] != preRngVal[0])
                    {
                        if (previewColorIndex == 0) previewColorIndex = 36;  //15,灰色;
                        else previewColorIndex = 0;
                    }
                }
                else
                {
                    if (rngVal != preRngVal)
                    {
                        if (previewColorIndex == 0) previewColorIndex = 36;  //15,灰色;
                        else previewColorIndex = 0;
                    }
                }

                lineArea.Interior.ColorIndex = previewColorIndex;

                previewRng = rng;
            }
            App.ScreenUpdating = true;
        }

        /// <summary>
        /// 移除首尾空白字符。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTrimBlankChars_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            Range usedRng = sheet.UsedRange;
            Range rng = Globals.ThisAddIn.Application.Intersect(usedRng, seledRng);
            if (rng == null || rng.Count <= 0)
            {
                MessageBox.Show("没有在已使用的区域选定任何单元格。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Globals.ThisAddIn.Application.ScreenUpdating = false;
            foreach (Range r in rng)
            {
                var text = r.Value as string;
                text = text.Trim();
                r.NumberFormatLocal = "@";
                r.Value2 = text;
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        /// <summary>
        /// 移除所有空白字符。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemoveBlankChars_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            Range usedRng = sheet.UsedRange;
            Range rng = Globals.ThisAddIn.Application.Intersect(usedRng, seledRng);
            if (rng == null || rng.Count <= 0)
            {
                MessageBox.Show("没有在已使用的区域选定任何单元格。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Globals.ThisAddIn.Application.ScreenUpdating = false;

            var sb = new StringBuilder();
            foreach (Range r in rng)
            {
                sb.Clear();

                var text = r.Value as string;
                r.NumberFormatLocal = "@";

                foreach (var c in text)
                {
                    if (char.IsWhiteSpace(c) == false)
                    {
                        sb.Append(c);
                    }
                }

                r.Value2 = sb.ToString();
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        /// <summary>
        /// 去除首尾空白字符，将中间连续空白字符替换为1个半角空格。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReplaceBlankChars_Click(object sender, RibbonControlEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (sheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            Range usedRng = sheet.UsedRange;
            Range rng = Globals.ThisAddIn.Application.Intersect(usedRng, seledRng);
            if (rng == null || rng.Count <= 0)
            {
                MessageBox.Show("没有在已使用的区域选定任何单元格。", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Globals.ThisAddIn.Application.ScreenUpdating = false;

            var sb = new StringBuilder();
            foreach (Range r in rng)
            {
                sb.Clear();

                var text = r.Value as string;
                r.NumberFormatLocal = "@";
                var isPreCharBlank = false;
                foreach (var c in text)
                {
                    if (char.IsWhiteSpace(c))
                    {
                        if (isPreCharBlank == false)
                        {
                            sb.Append(" ");
                            isPreCharBlank = true;
                        }
                        else continue;
                    }
                    else
                    {
                        sb.Append(c);
                        isPreCharBlank = false;
                    }
                }

                r.Value2 = sb.ToString().Trim();
            }

            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        private TagDialog tagDialog = new TagDialog();

        private void btnTags_Click(object sender, RibbonControlEventArgs e)
        {
            tagDialog.Show();
        }

        private void btnAddPageCutter_Click(object sender, RibbonControlEventArgs e)
        {
            var inputBox = new InputBox();
            if (inputBox.ShowDialog() != DialogResult.Yes) return;

            string[] keys = inputBox.InputedText.Split(new char[] { '|', }, StringSplitOptions.None);  // 要保留空格、空字符串等！

            if (inputBox.IgnoreBlanks)
            {
                for (int i = 0; i < keys.Length; i++)
                {
                    keys[i] = keys[i].Trim();
                }
            }

            if (inputBox.IgnoreCase)
            {
                for (int i = 0; i < keys.Length; i++)
                {
                    keys[i] = keys[i].ToLower();
                }
            }

            Worksheet actSheet = Globals.ThisAddIn.Application.ActiveSheet;
            if (actSheet == null) return;

            Range seledRng = Globals.ThisAddIn.Application.Selection;
            Range usedRng = actSheet.UsedRange;
            Range rng = Globals.ThisAddIn.Application.Intersect(usedRng, seledRng);
            if (rng == null || rng.Count <= 0)
            {
                MessageBox.Show("没有在已使用的区域选定任何单元格。", "LunarExcelAddIn", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            actSheet.ResetAllPageBreaks();

            foreach (Range cell in rng.Cells)
            {
                string cellVal;
                if (cell.Value2 == null)
                {
                    cellVal = "";
                }
                else
                {
                    cellVal = (inputBox.IgnoreCase ? cell.Value2.ToString().ToLower() : cell.Value2.ToString());
                }

                if (inputBox.IgnoreBlanks)
                {
                    cellVal = cellVal.Trim();
                }

                if (cell.Row > 1 && InKeys(keys, cellVal))
                {
                    actSheet.HPageBreaks.Add(cell);
                }
            }
        }

        private bool InKeys(string[] keys, string cellVal)
        {
            if (keys == null || keys.Length <= 0) return false;

            foreach (var key in keys)
            {
                if (string.IsNullOrEmpty(key) && string.IsNullOrEmpty(cellVal)) return true;

                if (cellVal == key) return true;
            }

            return false;
        }

        private void btnResetPageBreaks_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.ActiveCell.Worksheet.ResetAllPageBreaks();
        }
    }
}
