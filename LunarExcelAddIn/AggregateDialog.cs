﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace LunarExcelAddIn
{
    public partial class AggregateDialog : Form
    {
        public AggregateDialog(bool autoAggregate = false)
        {
            InitializeComponent();

            this.autoAggregate = autoAggregate;

            //读取所有工作表的名称

            int i = 0;
            int selSheetIndex = 0;
            foreach (Worksheet sht in Globals.ThisAddIn.Application.Sheets)
            {
                i++;
                cmbSheetNames.Items.Add(sht.Name);
                if (Globals.ThisAddIn.Application.ActiveSheet == sht)
                {
                    selSheetIndex = i;
                    sourceSheet = sht;
                }
            }

            cmbSheetNames.SelectedIndex = selSheetIndex - 1;
        }

        private Worksheet sourceSheet = null;

        private bool autoAggregate = false;

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public static Microsoft.Office.Interop.Excel.Application App { get { return Globals.ThisAddIn.Application; } }

        private void btnAggregate_Click(object sender, EventArgs e)
        {
            if (sourceSheet == null)
            {
                MessageBox.Show("当前没有打开可供查询的工作表！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (sourceSheet.UsedRange.Count <= 0)
            {
                MessageBox.Show("选定的表中没有可供查询的数据！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cmbClassFeild.SelectedIndex < 0)
            {
                MessageBox.Show("请选中至少一列作为【基准分类字段】！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cmbNumberFeild.SelectedIndex < 0)
            {
                MessageBox.Show("请选中至少一列作为汇总【数值字段】！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            btnAggregate.Enabled = false;

            //从哪一列开始，用户选择这一列必须准确，否则结果就不全。
            int startRowIndex = (int)numericUpDown1.Value + 1;
            int classFieldIndex = cmbClassFeild.SelectedIndex;  //这里要用到的数组索引从1开始算
            int numberFieldIndex = cmbNumberFeild.SelectedIndex;

            if (classFieldIndex == numberFieldIndex)
            {
                if (cmbNumberFeild.SelectedIndex < 0)
                {
                    MessageBox.Show("用于分类汇总的【基准字段】和【数值字段】不能是同一个字段！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnAggregate.Enabled = true;
                    return;
                }
            }

            List<AggregateItem> lstAggregateItems = new List<AggregateItem>();

            int usedRangeBottomIndex = sourceSheet.UsedRange.Rows.Count + sourceSheet.UsedRange.Row - 1;
            int baseColumnIndex = sourceSheet.UsedRange.Column + classFieldIndex;
            int numberColumnIndex = sourceSheet.UsedRange.Column + numberFieldIndex;

            if (usedRangeBottomIndex <= startRowIndex)
            {
                MessageBox.Show("字段标题行位置太低，已超出了可有区域！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            System.Array baseArray = sourceSheet.Range[sourceSheet.Cells[startRowIndex, baseColumnIndex], sourceSheet.Cells[usedRangeBottomIndex, baseColumnIndex]].Value;
            System.Array numberArray = sourceSheet.Range[sourceSheet.Cells[startRowIndex, numberColumnIndex], sourceSheet.Cells[usedRangeBottomIndex, numberColumnIndex]].Value;

            if (baseArray.Length != numberArray.Length)
            {
                MessageBox.Show("两列数据的数量不一致！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            #region [废弃]效率太低

            //for (int i = startRowIndex; i < (sourceSheet.UsedRange.Rows.Count + sourceSheet.UsedRange.Row); i++)
            //{
            //    Range cellBase = sourceSheet.Cells[i, sourceSheet.UsedRange.Column + classFieldIndex];
            //    Range cellNumber = sourceSheet.Cells[i, sourceSheet.UsedRange.Column + numberFieldIndex];

            //    if (cellBase == null || cellBase.Value == null) continue;

            //    //sb.Append($"基准字段值：{cellBase.Value}； 数值字段值：{cellNumber.Value}。\r\n");
            //    var newItem = new AggregateItem()
            //    {
            //        BaseFieldValue = cellBase.Value.ToString(),
            //    };

            //    if (App.WorksheetFunction.IsNumber(cellNumber) == false)
            //    {
            //        if (cellNumber == null || cellNumber.Value == null)
            //        {
            //            newItem.NumberValue = decimal.Zero;
            //        }
            //        else
            //        {
            //            var cellText = FormatCellText(cellNumber.Value.ToString());
            //            if (decimal.TryParse(cellText, out decimal realValue))
            //            {
            //                newItem.NumberValue = realValue;
            //            }
            //            else
            //            {
            //                newItem.NumberValue = decimal.Zero;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        newItem.NumberValue = new decimal(cellNumber.Value);
            //    }

            //    lstAggregateItems.Add(newItem);
            //}
            #endregion

            for (int i = 1; i <= baseArray.Length; i++)
            {
                var newItem = new AggregateItem();

                var cellValue = baseArray.GetValue(new int[] { i, 1, });    // 实际上是一维数组，但得用二维的方式
                if (cellValue != null)           // 有时候用户不会填写分类（例如没填班级）此时就取不出值。
                {
                    newItem.BaseFieldValue = cellValue.ToString();
                }
                //else
                //{
                //    newItem.BaseFieldValue = "";        // 后面是用 null 判断的，不能写死为空字符串。
                //}

                string cellText;

                var cell = numberArray.GetValue(new int[] { i, 1, });
                if (cell == null)
                {
                    cellText = "";
                }
                else
                {
                    cellText = cell.ToString();
                }

                newItem.TextValue = cellText;

                if (decimal.TryParse(cellText, out decimal result))    // 实际上是一维数组，但得用二维的方式
                {
                    newItem.NumberValue = result;
                }
                else
                {
                    newItem.NumberValue = decimal.Zero;
                }

                newItem.HasValue = (string.IsNullOrWhiteSpace(cellText) == false);

                lstAggregateItems.Add(newItem);
            }

            if (autoAggregate)
            {
                lstAggregateItems.Sort(new AutoAggregateComparer());
            }

            dataGridView1.Rows.Clear();//清空上次查询出来的数据。
            dataGridView1.ColumnCount = 2;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.Tomato;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font =
                new System.Drawing.Font(dataGridView1.Font, FontStyle.Bold);

            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;

            var baseColumn = dataGridView1.Columns[0];
            baseColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            baseColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            baseColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            if (dataGridView1.Columns.Count >= 2)
            {
                dataGridView1.Columns[0].HeaderText = cmbClassFeild.Text;

                if (rbtnSum.Checked)
                {
                    dataGridView1.Columns[1].HeaderText = cmbNumberFeild.Text + "[求和]";
                }
                else if (rbtnCount.Checked)
                {
                    dataGridView1.Columns[1].HeaderText = cmbNumberFeild.Text + "[计数]";
                }
                else if (rbtnJoin.Checked)
                {
                    dataGridView1.Columns[1].HeaderText = cmbNumberFeild.Text + "[连接]";
                }
                else
                {
                    dataGridView1.Columns[1].HeaderText = "未知字段";
                }
            }

            dataGridView1.TopLeftHeaderCell.Value = "No.";

            string preBaseValue = null;
            decimal number = 0;
            var fieldValuesCount = 0;
            var rowCount = 0;

            if (rbtnSum.Checked)
            {
                for (int j = 0; j < lstAggregateItems.Count; j++)
                {
                    var ai = lstAggregateItems[j];
                    if (string.IsNullOrWhiteSpace(preBaseValue) && j == 0)
                    {
                        preBaseValue = ai.BaseFieldValue;
                        fieldValuesCount++;
                        number += ai.NumberValue;
                        continue;
                    }

                    if (ai.BaseFieldValue == preBaseValue)
                    {
                        number += ai.NumberValue;
                    }
                    else
                    {
                        string[] newRow = new string[2];
                        newRow[0] = preBaseValue;
                        newRow[1] = number.ToString();

                        dataGridView1.Rows.Add(newRow);
                        rowCount++;

                        preBaseValue = ai.BaseFieldValue;
                        fieldValuesCount++;
                        number = ai.NumberValue;
                    }
                }

                // 最后一行
                if (rowCount == fieldValuesCount - 1)
                {
                    string[] newRow2 = new string[2];
                    newRow2[0] = preBaseValue;
                    newRow2[1] = number.ToString();

                    dataGridView1.Rows.Add(newRow2);
                }
            }
            else if (rbtnCount.Checked)
            {
                var valuesCount = 0;
                for (int j = 0; j < lstAggregateItems.Count; j++)
                {
                    var ai = lstAggregateItems[j];
                    if (string.IsNullOrWhiteSpace(preBaseValue) && j == 0)
                    {
                        preBaseValue = ai.BaseFieldValue;
                        fieldValuesCount++;
                        if (ai.HasValue)
                            valuesCount++;
                        continue;
                    }

                    if (ai.BaseFieldValue == preBaseValue)
                    {
                        if (ai.HasValue)
                            valuesCount++;
                    }
                    else
                    {
                        string[] newRow = new string[2];
                        newRow[0] = preBaseValue;
                        newRow[1] = valuesCount.ToString();

                        dataGridView1.Rows.Add(newRow);
                        rowCount++;

                        preBaseValue = ai.BaseFieldValue;
                        fieldValuesCount++;

                        if (ai.HasValue)
                        {
                            valuesCount = 1;
                        }
                        else
                        {
                            valuesCount = 0;
                        }
                    }
                }

                // 最后一行
                if (rowCount == fieldValuesCount - 1)
                {
                    string[] newRow2 = new string[2];
                    newRow2[0] = preBaseValue;
                    newRow2[1] = valuesCount.ToString();

                    dataGridView1.Rows.Add(newRow2);
                }
            }
            else if (rbtnJoin.Checked == true)
            {
                var text = "";

                for (int j = 0; j < lstAggregateItems.Count; j++)
                {
                    var ai = lstAggregateItems[j];
                    if (string.IsNullOrWhiteSpace(preBaseValue) && j == 0)
                    {
                        preBaseValue = ai.BaseFieldValue;
                        fieldValuesCount++;
                        text += /*tbxJoinChars.Text +*/ ai.TextValue;
                        continue;
                    }

                    if (ai.BaseFieldValue.ToLower() == preBaseValue.ToLower())
                    {
                        text += tbxJoinChars.Text + ai.TextValue;
                    }
                    else
                    {
                        string[] newRow = new string[2];
                        newRow[0] = preBaseValue;
                        newRow[1] = text;

                        dataGridView1.Rows.Add(newRow);
                        rowCount++;

                        preBaseValue = ai.BaseFieldValue;
                        fieldValuesCount++;
                        text = ai.TextValue;
                    }
                }

                // 最后一行
                if (rowCount == fieldValuesCount - 1)
                {
                    string[] newRow2 = new string[2];
                    newRow2[0] = preBaseValue;
                    newRow2[1] = text;

                    dataGridView1.Rows.Add(newRow2);
                }
            }

            if (dataGridView1.Rows.Count > 0)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    dataGridView1.Rows[i].HeaderCell.Value = (i + 1).ToString();
                }
            }

            btnAggregate.Enabled = true;
        }

        private string FormatCellText(string src)
        {
            return src.Replace(",", "").Replace("，", "").Replace("－", "-").Replace("。", ".").Replace("１", "1").Replace("２", "2").Replace("３", "3").Replace("４", "4").Replace("５", "5")
                   .Replace("６", "6").Replace("７", "7").Replace("８", "8").Replace("９", "9").Replace("０", "0").Replace("　", "").Replace(" ", "");
        }

        private void btnPasteToNewSheet_Click(object sender, EventArgs e)
        {
            var newSheet = (Worksheet)Globals.ThisAddIn.Application.Sheets.Add(Globals.ThisAddIn.Application.ActiveSheet);

            newSheet.Cells.NumberFormatLocal = "@";

            //VBA Sample Code:
            //    Range("D14").Select
            //    ActiveSheet.PasteSpecial Format:= "HTML", Link:= False, DisplayAsIcon:= _
            //        False, NoHTMLFormatting:= True
            dataGridView1.SelectAll();

            var data = dataGridView1.GetClipboardContent();

            if (data == null)
            {
                MessageBox.Show("没有内容可以粘贴！", "LunarExcelAddIn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Clipboard.SetDataObject(data);

            Globals.ThisAddIn.Application.ScreenUpdating = false;

            newSheet.Cells[1, 1].Select();
            newSheet.PasteSpecial("HTML", false, false, null, null, null, true);
            //为什么使用这个方法？
            //因为这里是从 DataGridView 向 Excel 粘贴数据，而不是在 Excel 工作表之间粘贴数据。

            var startRowIndex = 1;
            // 可能出现单元格内换行的情况，这时候，粘贴的标题部分会变成多行。所以要算清有几行标题。
            startRowIndex += (newSheet.UsedRange.Rows.Count - dataGridView1.RowCount);

            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        private void cmbSheetNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            sourceSheet = Globals.ThisAddIn.Application.Sheets[cmbSheetNames.SelectedIndex + 1];
            var usedColumnCount = sourceSheet.UsedRange.Columns.Count;
            if (usedColumnCount <= 0 || sourceSheet.UsedRange.Count <= 0)
            {
                MessageBox.Show($"表 【{cmbSheetNames.Text}】 中没有可供查询的数据！", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            numericUpDown1.Maximum = sourceSheet.UsedRange.Row + sourceSheet.UsedRange.Rows.Count - 1;
            numericUpDown1.Value = sourceSheet.UsedRange.Row;
            //FillCellsToListBox();//没必要
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            FillCellsToListBox();
        }


        private void FillCellsToListBox()
        {
            cmbClassFeild.Items.Clear();
            cmbNumberFeild.Items.Clear();

            if (sourceSheet == null) return;
            if (cmbSheetNames.SelectedIndex < 0) return;
            if (numericUpDown1.Value < sourceSheet.UsedRange.Row) return;

            Range titleCells = sourceSheet.Cells[numericUpDown1.Value, 1];
            titleCells = Globals.ThisAddIn.Application.Intersect(sourceSheet.UsedRange, titleCells.EntireRow);

            if (titleCells != null && titleCells.Count > 0)
            {
                foreach (Range cell in titleCells)
                {
                    if (cell == null) continue;//可能存在合并单元格

                    string columnTitleText;
                    if (cell.Value == null) columnTitleText = "[空列标题]";
                    else columnTitleText = cell.Value.ToString();

                    cmbClassFeild.Items.Add(columnTitleText);
                    cmbNumberFeild.Items.Add(columnTitleText);
                }
            }

            if (cmbClassFeild.Items.Count > 0)
            {
                cmbClassFeild.SelectedIndex = 0;
            }

            if (cmbNumberFeild.Items.Count > 1)
            {
                cmbNumberFeild.SelectedIndex = 1;
            }
        }

        private void AggregateDialog_Load(object sender, EventArgs e)
        {
            numericUpDown1_ValueChanged(sender, e);
        }

        private void rbtnJoin_CheckedChanged(object sender, EventArgs e)
        {
            tbxJoinChars.Enabled = rbtnJoin.Checked;
        }
    }

    internal class AutoAggregateComparer : IComparer<AggregateItem>
    {
        public int Compare(AggregateItem x, AggregateItem y)
        {
            return x.BaseFieldValue.CompareTo(y.BaseFieldValue);
        }
    }

    public struct AggregateItem
    {
        /// <summary>
        /// 对应字段名。
        /// </summary>
        public string BaseFieldValue { get; set; }

        /// <summary>
        /// 用于求和型汇总。
        /// </summary>
        public decimal NumberValue { get; set; }

        /// <summary>
        /// 用于连接型汇总。
        /// </summary>
        public string TextValue { get; set; }

        /// <summary>
        /// 不问类型，用于计数型汇总。
        /// </summary>
        public bool HasValue { get; set; }
    }
}
