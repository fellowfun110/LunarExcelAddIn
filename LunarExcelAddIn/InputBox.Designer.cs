﻿
namespace LunarExcelAddIn
{
    partial class InputBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnInsertPageBreaks = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ckxIgnoreBlanks = new System.Windows.Forms.CheckBox();
            this.tbxText = new System.Windows.Forms.TextBox();
            this.ckxInnoreCase = new System.Windows.Forms.CheckBox();
            this.btnResetPageBreaks = new System.Windows.Forms.Button();
            this.ckxAutoResetPageBreaks = new System.Windows.Forms.CheckBox();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 159);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 25, 0);
            this.statusStrip1.Size = new System.Drawing.Size(575, 22);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "用指定的正则表达式验证当前选中的单元格中的文本内容。";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(151, 17);
            this.toolStripStatusLabel1.Text = "以 | 分隔多个关键词！！！";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnClose.Location = new System.Drawing.Point(427, 113);
            this.btnClose.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(133, 32);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "关闭(&X)";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnInsertPageBreaks
            // 
            this.btnInsertPageBreaks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInsertPageBreaks.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnInsertPageBreaks.Location = new System.Drawing.Point(283, 113);
            this.btnInsertPageBreaks.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.btnInsertPageBreaks.Name = "btnInsertPageBreaks";
            this.btnInsertPageBreaks.Size = new System.Drawing.Size(133, 32);
            this.btnInsertPageBreaks.TabIndex = 8;
            this.btnInsertPageBreaks.Text = "插入新分页符(&V)";
            this.btnInsertPageBreaks.UseVisualStyleBackColor = true;
            this.btnInsertPageBreaks.Click += new System.EventHandler(this.btnInsertPageBreaks_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "请输入单元格开头文本：";
            // 
            // ckxIgnoreBlanks
            // 
            this.ckxIgnoreBlanks.AutoSize = true;
            this.ckxIgnoreBlanks.Location = new System.Drawing.Point(401, 81);
            this.ckxIgnoreBlanks.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckxIgnoreBlanks.Name = "ckxIgnoreBlanks";
            this.ckxIgnoreBlanks.Size = new System.Drawing.Size(159, 24);
            this.ckxIgnoreBlanks.TabIndex = 11;
            this.ckxIgnoreBlanks.Text = "忽略首尾空白字符(&B)";
            this.ckxIgnoreBlanks.UseVisualStyleBackColor = true;
            // 
            // tbxText
            // 
            this.tbxText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxText.Location = new System.Drawing.Point(24, 43);
            this.tbxText.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbxText.Name = "tbxText";
            this.tbxText.Size = new System.Drawing.Size(536, 26);
            this.tbxText.TabIndex = 12;
            // 
            // ckxInnoreCase
            // 
            this.ckxInnoreCase.AutoSize = true;
            this.ckxInnoreCase.Location = new System.Drawing.Point(230, 79);
            this.ckxInnoreCase.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckxInnoreCase.Name = "ckxInnoreCase";
            this.ckxInnoreCase.Size = new System.Drawing.Size(118, 24);
            this.ckxInnoreCase.TabIndex = 11;
            this.ckxInnoreCase.Text = "忽略大小写(&U)";
            this.ckxInnoreCase.UseVisualStyleBackColor = true;
            // 
            // btnResetPageBreaks
            // 
            this.btnResetPageBreaks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetPageBreaks.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnResetPageBreaks.Location = new System.Drawing.Point(24, 76);
            this.btnResetPageBreaks.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.btnResetPageBreaks.Name = "btnResetPageBreaks";
            this.btnResetPageBreaks.Size = new System.Drawing.Size(156, 32);
            this.btnResetPageBreaks.TabIndex = 13;
            this.btnResetPageBreaks.Text = "清除现有分页符(&C)";
            this.btnResetPageBreaks.UseVisualStyleBackColor = true;
            this.btnResetPageBreaks.Click += new System.EventHandler(this.btnResetPageBreaks_Click);
            // 
            // ckxAutoResetPageBreaks
            // 
            this.ckxAutoResetPageBreaks.AutoSize = true;
            this.ckxAutoResetPageBreaks.Location = new System.Drawing.Point(24, 118);
            this.ckxAutoResetPageBreaks.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckxAutoResetPageBreaks.Name = "ckxAutoResetPageBreaks";
            this.ckxAutoResetPageBreaks.Size = new System.Drawing.Size(146, 24);
            this.ckxAutoResetPageBreaks.TabIndex = 11;
            this.ckxAutoResetPageBreaks.Text = "插入前自动重置(&A)";
            this.ckxAutoResetPageBreaks.UseVisualStyleBackColor = true;
            // 
            // InputBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 181);
            this.Controls.Add(this.btnResetPageBreaks);
            this.Controls.Add(this.tbxText);
            this.Controls.Add(this.ckxAutoResetPageBreaks);
            this.Controls.Add(this.ckxInnoreCase);
            this.Controls.Add(this.ckxIgnoreBlanks);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnInsertPageBreaks);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "InputBox";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "输入框";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnInsertPageBreaks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox ckxIgnoreBlanks;
        private System.Windows.Forms.TextBox tbxText;
        private System.Windows.Forms.CheckBox ckxInnoreCase;
        private System.Windows.Forms.Button btnResetPageBreaks;
        private System.Windows.Forms.CheckBox ckxAutoResetPageBreaks;
    }
}